﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Segmented.DataModel;

namespace Skulls {
    static public class Config {

        static public Context Context { get ; set; }
        

        static public int GetInt(string id) {
            return Get<int>(id);
        }

        static public float GetFloat(string id) {
            return Get<float>(id);
        }

        static private Segmented.DataModel.RecursiveValueInjector vi = new Segmented.DataModel.RecursiveValueInjector();

        static public T Get<T>(string key) {
            object o;
            if (vars.TryGetValue(key, out o)) {
                if (o is Node) {                
                    var result = (T)vi.Inject(typeof(T), (Node)o);
                    vars[key] = result;
                    return result;
                } else {
                    return (T)o;
                }
            }
            
            
            return default(T);
            
        }

        internal static void Add(string key, Node node) {
            vars[key] = node;
        }

        static private Dictionary<string, object> vars = new Dictionary<string, object>();

        internal static RLNET.RLColor GetColor(string s) {
            return Get<RLNET.RLColor>(s);
            
        }
    }
}
