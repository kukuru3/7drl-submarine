﻿using Skulls.Logic.Map;
using Skulls.Logic;
using Skulls.Logic.Generators;
using Skulls.Rendering;
using Skulls.Pipeline;

namespace Skulls {
    public class Context {

        public Renderer          Renderer   { get; private set; }
        public WorldGenerator    Generator  { get; private set; }
        public WorldMap          Map        { get; private set; }
        public RenderingPipeline Pipeline   { get; private set; }
        public GUI.GUIManager    GUI        { get; private set; }
        public SkullsGame        Game       { get; private set; }
        public EntityManager     Entities   { get; private set; }
        public EntityGenerator   EntityGenerator { get; private set; }
        public WorldSim          Simulator  { get; private set; }
        public Mnemhotep.GenerationContext MnemhotepGenerator { get; private set; }        
        
        public Definitions       Definitions { get; internal set; }

        public Context(SkullsGame game) {

            Game = game;
            Renderer            = new Renderer(this);
            Pipeline            = new RenderingPipeline(this);
            Map                 = new WorldMap(this);
            Generator           = new WorldGenerator(this);
            Definitions         = new Definitions();
            Entities            = new EntityManager(this);
            EntityGenerator     = new EntityGenerator(this);
            Simulator           = new WorldSim(this);
            

            Config.Context = this;

            var mnemhotepInstance = new Mnemhotep.Mnemhotep();
            MnemhotepGenerator  = new Mnemhotep.GenerationContext(false);
                
            GUI         = new Skulls.GUI.GUIManager(this);
        }

        internal void AssignWorld(WorldMap map) {
            Map = map;
            Generator.World = Map;                        
            Generator.SectorManager.Map = Map;            
            var context = new Mnemhotep.GenerationContext(true);
            
        }
    }
}
