﻿using System;
using System.Collections.Generic;
using System.Linq;
using Skulls.Logic;

namespace Skulls.Data {
    public class Blueprint {
        public enum Classes {
            None,
            Vessel,
            Creature,
        }

        public string ID { get; internal set; }
        public Classes Class { get; internal set; }

        public SpecDef Specs;
        public Dictionary<string, string> Data;
        public Segmented.DataModel.Node ComponentsNode;

        public class SpecDef {
            /// <summary> Do not use these directly </summary>
            public string[]                 AllowedVelocities;
            /// <summary> Do use these. </summary>
            public Definitions.Velocity[]   Velocities;
            public float EngineOutput;
            public int   Maneuver;

            /// <summary>Grossly inefficient, added late in the cycle. I am shame.</summary>
            /// <remarks>Must commit sudoku</remarks>
            internal int GetVelocityClassByName(string str) {
                return Array.IndexOf(Velocities, Velocities.First(v => v.name == str));
            }
        }

        internal void CompleteParse(Definitions defs) {
            if (Specs == null) return;
            Specs.Velocities = Specs.AllowedVelocities.Select(strid => defs.GetVelocityDefinition(strid) ).ToArray();
        }
    }
}
