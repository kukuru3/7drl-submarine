﻿using System;
using System.Collections.Generic;
using Segmented.DataModel;

namespace Skulls.Data {
    
    [Segmented.Converter(typeof(RLNET.RLColor))]
    internal class ColorConverter : Segmented.IDataNodeConverter {
        
        public object ConvertDataNode(Node node) {
            var source = node.Value;
            return Convert(source);
           
        }

        static public RLNET.RLColor Convert(string source) {
             if (source.Length == 3) source = new string( new char[] { source[0] , source[0] ,  source[1] ,  source[1] , source[2] , source[2]});
             if (source.Length == 6) {
                var r = uint.Parse(source.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                var g = uint.Parse(source.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
                var b = uint.Parse(source.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
                
                return new RLNET.RLColor((byte)r,(byte)g,(byte)b);
                //return 0xff000000 | (r << 16) | (g << 8) | b;
            }
             return RLNET.RLColor.Black;
        }
    }

}
