﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ur.Filesystem;

namespace Skulls.Data {
    public class DefinitionsLoader {

        Segmented.DataModel.Node rootNode;

        void Load() {
            if (rootNode != null) return;
            var assets = Folders.GetDirectory("Assets");  
            var p = new Segmented.Parse.YamlParser();
            var filePath = System.IO.Path.Combine(assets, "defs.yml" );
            rootNode = p.ParseYamlData( filePath );
        }

        internal RLNET.RLSettings LoadSettings() {
            Load();
            var vi = new Segmented.DataModel.RecursiveValueInjector();
            var result = vi.Inject(typeof(RLNET.RLSettings), rootNode.Get("settings")) as RLNET.RLSettings;
            return result;
            
        }

        internal void LoadConfig() {
            Load();
            var vi = new Segmented.DataModel.RecursiveValueInjector();
            foreach (var child in rootNode.Get("config").Children) {
                var key = child.ID;
                var value = child;
                Config.Add(key, value);
            }
        }

        internal Logic.Definitions LoadDefinitions() {
            
            Load();

            var vi = new Segmented.DataModel.RecursiveValueInjector();
            var result = vi.Inject(typeof(Logic.Definitions), rootNode.Get("definitions" ));
            
            defs = result as Logic.Definitions;
            defs?.Finish();
            return defs;
        }

        Logic.Definitions defs;

        internal void LoadBlueprints() {
            Load();
            var vi = new Segmented.DataModel.RecursiveValueInjector();
            foreach (var child in rootNode.Get("blueprints").Children) {
                var bpid = child.ID;
                var bp = vi.Inject(typeof(Blueprint), child) as Blueprint;
                bp.ID = bpid;
                bp.ComponentsNode = child.Get("Components");
                bp.CompleteParse(defs);
                defs.AddBlueprint(bp);
            }
        }

        internal void LoadRexPacks() {

            Load();

            var rexloader = new RexLoader();

            foreach (var child in rootNode.Get("rex_packs").Children) {
                var id = child.ID;
                var f = Folders.GetDirectory("Assets\\Packs\\");
                var path = f + child.Value;

                rexloader.Load(path, id);

            }

        }

        internal void LoadKeyBindings() {
            Load();
            
            foreach (var child in rootNode.Get("keybindings").Children) {
                var keyID       = child.ID;
                var commandVal  = child.Value;

                RLNET.RLKey key;
                if (Enum.TryParse(keyID, true, out key)) {
                    defs.AddKeybinding(key, commandVal);
                }
            }
        }
    }
}
