﻿using System;
using System.Collections.Generic;
using Mnemhotep;
using System.IO;

namespace Skulls.Data {
    public class MnemhotepLoader {
        /// <summary> Loads definitions from disk and adds them to mnemhotep instance</summary>        
        internal void DoLoad(Mnemhotep.Mnemhotep mnemhotepGenerator) {
            var folder = Skulls.Logic.Serialization.FileOps.GetSourceFolder();
            var f = System.IO.Path.Combine(folder, "Assets\\Dictionaries");
            var di = new DirectoryInfo(f);
            if (di.Exists) {
                var m = new Mnemhotep.Mnemhotep();            
                m.ProcessFolder(di.FullName);
            }
            
        }
    }
}
