﻿using System;
using System.Collections.Generic;

namespace Skulls.Data {
    public class RexLoader {
        
        public void Load(string path, string id) {
            
            var rr = new RexTools.RexReader(path);
            var rp = RexPack.FromMap(rr.GetMap());
            rp.Register(id);
        }        
    }
}
