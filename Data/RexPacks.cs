﻿using System;
using System.Collections.Generic;
using Ur.Grid;
using Ur;
using RexTools;
using RLNET;

namespace Skulls.Data {

    public struct RexTile {
        public RLColor Back { get; }
        public RLColor Fore { get; }
        public char    Char { get; }

        public RexTile(char c, byte back_r, byte back_g, byte back_b, byte fore_r, byte fore_g, byte fore_b ) {
            Char = c;
            Back = new RLColor(back_r, back_g, back_b);
            Fore = new RLColor(fore_r, fore_g, fore_b);
        }

    }

    public class RexPack {

        public RexTile[,] tiles;

        static public RexPack FromMap(TileMap map) {
            var tt = map.Layers[0].Tiles;
            var rp = new RexPack();
            // rex loader uses a Y-first indexing, we use X-first, so we might as well flip them around here at no extra cost
            rp.tiles = new RexTile[tt.GetLength(1), tt.GetLength(0)];
            foreach (var t in tt.Iterate()) {                
                var nt = tt[t.X, t.Y];
                rp.tiles[t.Y, t.X] = new RexTile(
                    (char)nt.CharacterCode, 
                    nt.BackgroundRed, nt.BackgroundGreen, nt.BackgroundBlue, 
                    nt.ForegroundRed, nt.ForegroundGreen, nt.ForegroundBlue
                    );
            }
            return rp;
        }

        public static RexPack Get(string id) {
            return RexPackManager.Instance.GetPack(id);
        }

        public void Register(string id) {
            RexPackManager.Instance.Register(id, this);
            
        }
        
    }

    public class RexPackManager {

        static private RexPackManager instance;
        static public RexPackManager Instance { get {  if (instance == null) instance = new RexPackManager(); return instance; } }


        Dictionary<string, RexPack> packs;

        private RexPackManager() {
            
            instance = this;
            packs = new Dictionary<string, RexPack>();

        }
                
        internal void Register(string packId, RexPack pack) {
            packs[packId] = pack;
        }

        internal RexPack GetPack(string id) {
            return packs[id];
        }
    }
}
