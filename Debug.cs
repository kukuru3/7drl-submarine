﻿using System;
using System.Collections.Generic;

namespace Skulls {
    public static class Debug {

        public static void Log(string log) {
            System.Diagnostics.Debug.WriteLine(log);
            Console.WriteLine(log);
        }

    }
}
