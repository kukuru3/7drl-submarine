﻿using System;
using System.Collections.Generic;

namespace Skulls.Errors {
    public class WorldSerializationException : Exception {
        public WorldSerializationException(string message) : base(message) {

        }
    }
}
