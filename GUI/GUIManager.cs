﻿using System;
using System.Collections.Generic;
using Skulls.Pipeline;
using Skulls.Pipeline.Views;
using System.Linq;
using Ur.Grid;
using RLNET;
using Skulls.Rendering;
using Skulls.Pipeline.ControlPanel;

namespace Skulls.GUI {
    public class GUIManager {

        public Context Context { get; }
        
        Logic.Generators.WorldGenerator Generator { get { return Context.Generator; } }
        
        public GUIManager(Context context) {
            Context = context;
        }

        internal GeneratorView CreateWorldGeneratorView() {
            var view = new GeneratorView(Generator);
            view.Position = new Coords(Context.Pipeline.RootElement.Rect.Width - 16, 1);
            view.Width = 15;
            view.Height = 10;            
            Context.Pipeline.Register(view);

            var win = new Window();
            win.Parent = view;
            win.Dimensions = win.Parent.Dimensions; // = new Rect(0, 0, win.Parent.LocalRect.Width, win.Parent.LocalRect.Height);
            win.BackColor = RLColor.Brown * 0.3f;
            Context.Pipeline.Register(win);

            var game = Context.Game;

            var spacer = win.CreateLabel("");
            
            var generateBtn = win.CreateButton("New World");
            generateBtn.OnClick = delegate() {
                var c = new Logic.Generators.WorldCreator();
                var gendefs = Context.Definitions.GeneratorDefinition;
                var world = c.CreateNewWorld(Context, 
                    gendefs.WorldSectorsWide, 
                    gendefs.WorldSectorsTall, 
                    gendefs.TilesPerSector
                );
                Context.AssignWorld(world);
                
                Context.Generator.WorldStarter.GenerateAll();
                view.Visible = !view.Visible;
            };

            var loadBtn = win.CreateButton("Previous W.");
            var guiMgr = this;

            loadBtn.OnClick = delegate() {
                guiMgr.CreateWorldFileList(win);
            };
            
            var deleteButton = win.CreateButton("Delete All");
            deleteButton.OnClick = delegate() {
                var c = new Logic.Generators.WorldCreator();
                foreach (var w in c.EnumerateExistingWorlds()) {
                    c.DestroyWorldFiles(w);
                }
            };
            
            return view;
        }

        internal Switch CreateSwitch(int x, int y, bool vertical, string signalID, Switch.Styles style = Switch.Styles.Default) {
            var s = new Switch(vertical, signalID, style);
            s.Parent = cockpit;
            s.Position = new Coords(x, y);
            Context.Pipeline.Register(s);
            return s;
        }

        internal KeyCommandGenerator CreateKeyboaardCommandGenerator() {
            var kg = new KeyCommandGenerator();
            Context.Pipeline.Register(kg);
            return kg;
        }

        internal ASCIIPicker CreateASCIIPicker() {
            var ap = new ASCIIPicker();
            ap.Position = new Coords(20, 20);
            ap.Dimensions = new Coords(68, 10);
            Context.Pipeline.Register(ap);
            ap.Visible = false;
            return ap;
        }

        internal FortuneCookie CreateFC() {
            var fc = new FortuneCookie();
            fc.Position = new Coords(0, Context.Pipeline.RootElement.Height - 1);
            fc.Width = 40;
            fc.Height = 1;            
            Context.Pipeline.Register(fc);
            return fc;
        }

        private Window CreateWorldFileList(PipelineElement parent) {
            var manager = this;
            var window = new Window();
            window.Dimensions = window.Parent.Dimensions;            
            window.BackColor = RLColor.Red;
            window.Parent = parent;
            Context.Pipeline.Register(window);
            window.CreateButton("[close]").OnClick = delegate() {
                manager.DestroyElement(window);
            };                       
            
            var c = new Logic.Generators.WorldCreator();
            foreach (var world_id in c.EnumerateExistingWorlds()) {
                var btn = window.CreateButton(Convert.ToString(world_id, 16));

                var generator = new Logic.Generators.WorldCreator();

                btn.tagData = world_id;

                btn.OnClick += delegate() {
                    manager.DestroyElement(window);
                    var wc = new Logic.Generators.WorldCreator();
                    wc.CreateWorldFromFile(Context, (long)btn.tagData);
                    Context.Generator.WorldStarter.GenerateAll();
                };

                btn.OnRClick += delegate() {
                    c.DestroyWorldFiles((long)btn.tagData);
                    DestroyElement(btn);
                };
            }
            
            return window;
        }

        private void DestroyElement(PipelineElement e) {
            Context.Pipeline.Destroy(e);
        }


        internal WorldView CreateWorldView() {
            var ww = new WorldView();
            ww.Position = new Coords(20, 0);
            ww.Width = ww.Height = 60; 
            Context.Pipeline.Register(ww);
            return ww;
        }
        
        Cockpit cockpit;

        internal Cockpit CreateCockpit() {

            cockpit = new Cockpit();
            cockpit.Width = Context.Renderer.Console.Width;
            cockpit.Height = Context.Renderer.Console.Height; 
            Context.Pipeline.Register(cockpit);

            var s = new Switch(true, "Pumps.1"); s.Position = new Coords(2,9); s.Parent = cockpit; Context.Pipeline.Register(s);
                s = new Switch(true, "Pumps.2"); s.Position = new Coords(4,9); s.Parent = cockpit; Context.Pipeline.Register(s);
                s = new Switch(true, "Pumps.3"); s.Position = new Coords(6,9); s.Parent = cockpit; Context.Pipeline.Register(s);

            #region AZIMT / TURBN

            var azimuthRotary = new Rotary(5, -1, 4, true);
            azimuthRotary.Ribbon = "000x030x060x090x120x150x180x210x240x270x300x330x".Replace('x', (char)Glyphs.TinyPoint);
            azimuthRotary.Parent = cockpit;
            azimuthRotary.Position = new Coords(2, 20); 
            Context.Pipeline.Register(azimuthRotary);

            var turbineRotary = new Rotary(5, -2, 8, false);
            turbineRotary.Ribbon = "0.1.2.3.4";
            turbineRotary.Parent = cockpit;
            turbineRotary.Position = new Coords(13, 20);
            Context.Pipeline.Register(turbineRotary);
            #endregion

            #region BRIDG
            var bridgeRotary = new Rotary(5, -2, 6, false);            
            bridgeRotary.Position = new Coords(2, 28);
            bridgeRotary.Ribbon = "-60 -45 -30 -15 000 +15 +30 +45 +60".Replace(' ', (char)Glyphs.TinyPoint);
            bridgeRotary.RotaryValue = 17;
            bridgeRotary.Parent = cockpit;
            Context.Pipeline.Register(bridgeRotary);

            CreateSwitch(12, 28, false, "Periscope", Switch.Styles.GreyNullGreenPlus);
            CreateSwitch(12, 30, false, "Reflector", Switch.Styles.GreyNullGreenPlus);
            CreateSwitch(12, 32, false, "Sonar", Switch.Styles.GreyNullGreenPlus);
            CreateSwitch(17, 28, false, "System4", Switch.Styles.GreyNullGreenPlus);
            CreateSwitch(17, 30, false, "System5", Switch.Styles.GreyNullGreenPlus);
            CreateSwitch(17, 32, false, "System6", Switch.Styles.GreyNullGreenPlus);

            #endregion


            #region Depth panel
            
            var DepthGauge = new LinearGauge(true, false, 3, 5);
            DepthGauge.SetColors();
            DepthGauge.SetColorFade(0, 0.65f);
            DepthGauge.SetRibbon(0, new string((char)Glyphs.TJunc_Left, 20));
            DepthGauge.SetRibbon(1, new string((char)Glyphs.SmallPoint, 20));
            DepthGauge.SetRibbon(3, new string((char)Glyphs.SmallPoint, 20));
            DepthGauge.SetRibbon(4, new string((char)Glyphs.TJunc_Right, 20));
            var sb = new System.Text.StringBuilder();
            sb.Append(' ', 3); sb.Append((char)Glyphs.SmallPoint, 14); sb.Append(' ', 3);
            sb[3] = (char)Glyphs.Cog; sb[4] = (char)Glyphs.WaveSingle; sb[7] = (char)Glyphs.WaveDouble; sb[10] = (char)Glyphs.ArrowDown;
            DepthGauge.SetRibbon(2, sb.ToString());

            DepthGauge.SetThresholds(3, 10);
            DepthGauge.AddOverlayChar(-1, 0, (char)Glyphs.TriangleRight);
            DepthGauge.AddOverlayChar( 1, 0, (char)Glyphs.TriangleLeft);
            DepthGauge.Parent = cockpit;
            DepthGauge.Position = new Coords(10, 9);
            DepthGauge.IndicatorIdentity = "DepthIndicator";
            Context.Pipeline.Register(DepthGauge);

            #endregion
            
            var SpeedGauge = new LinearGauge(false, false, 7, 3);

            SpeedGauge.SetColors();
            SpeedGauge.SetColorFade(1, 0.65f);

            var sbl = new System.Text.StringBuilder();
            sbl.Append((char)Glyphs.TJunc_Top, 29);
            SpeedGauge.SetRibbon(0, sbl.ToString());

            sbl.Clear(); sbl.Append( "    0   10  20  30  40  50    ");            
            SpeedGauge.SetRibbon(1, sbl.ToString());

            sbl.Clear(); sbl.Append((char)Glyphs.TJunc_Bottom, 29);
            SpeedGauge.SetRibbon(2, sbl.ToString());
                        
            SpeedGauge.SetThresholds(4, 24);
                        
            SpeedGauge.AddOverlayChar(0, -1, (char)31);
            SpeedGauge.AddOverlayChar(0,  1, (char)30);            

            SpeedGauge.Parent = cockpit;

            SpeedGauge.Position = new Coords(12, 16);
            SpeedGauge.IndicatorIdentity = "SpeedIndicator";
            Context.Pipeline.Register(SpeedGauge);

            var AzimuthGauge = new LinearGauge(false, true, 7, 3);
            AzimuthGauge.SetColors();
            AzimuthGauge.SetColorFade(1, 0.65f);

            var s0 = new string((char)Glyphs.TJunc_Top, 72);
            var s2 = new string((char)Glyphs.TJunc_Bottom, 72);
            AzimuthGauge.SetRibbon(0, s0);
            AzimuthGauge.SetRibbon(1, "-N- 015 030 045 060 075 -E- 105 120 135 150 165 -S- 195 210 225 240 255 -W- 285 300 315 330 345 ");
            AzimuthGauge.SetRibbon(2, s2);

            AzimuthGauge.AddOverlayChar(0, -1, (char)31);
            AzimuthGauge.AddOverlayChar(0,  1, (char)30);
            AzimuthGauge.SetThresholds(0, 71);

            AzimuthGauge.Position = new Coords(2, 16);
            AzimuthGauge.Parent = cockpit;

            Context.Pipeline.Register(AzimuthGauge);

            return cockpit;
        }

    }
}
