﻿using System;
using System.Collections.Generic;

namespace Skulls {
    
    public enum Glyphs {
        
        SubcellLeft = 221,
        SubcellRight = 222,
        SubcellTop   = 223,
        SubcellBottom = 220,
        
        TinyPoint = 250,
        SmallPoint = 249,
        LargePoint = 7,
        Star = 42,
        Cog = 15,
        
        WaveSingle = 126,
        WaveDouble = 247,
        
        OneHalfSymbol = 171,
        OneQuarterSymbol = 172,
        
        LineHorizontal = 196,
        LineVertical = 179,
        Corner_UpperLeft,
        Corner_LowerLeft  = 192,
        Corner_UpperRight = 191,
        Corner_LowerRight = 217,
        TJunc_Bottom = 193,
        TJunc_Top = 194,
        TJunc_Left = 195,
        TJunc_Right = 180,
        Cross = 197 ,
        
        Double_LineHorizontal = 205,
        Double_LineVertical = 186,
        Double_Corner_UpperLeft = 201,
        Double_Corner_LowerLeft = 200,
        Double_Corner_UpperRight = 187,
        Double_Corner_LowerRight = 188,
        Double_TJunc_Bottom = 202,
        Double_TJunc_Top = 203,
        Double_TJunc_Left = 204,
        Double_TJunc_Right = 185,
        Double_Cross = 206,
        
        ArrowUp = 24,
        ArrowDown = 25,
        ArrowLeft = 27,
        ArrowRight = 26,

        ArrowLeftRight = 29,
        ArrowUpDown = 18,

        TriangleUp = 30,
        TriangleDown = 31,
        TriangleLeft = 17,
        TriangleRight = 16,

        House = 127,
        Fish  = 224,

        VGaugeFull = 160,
        VGaugeThreeQuarters = 161,
        VGaugeHalf = 162,
        VGaugeQuarter = 163,

        HGaugeQuarter = 164,
        HGaugeThreeQuarters = 165,
        HGaugeHalf = SubcellLeft,
        
        MidlineVerticalSubcell   = VGaugeFull,
        MidlineHorizontalSubcell = 166,
        Vertical4Stack = 167,

        Periscope = 244,

        TwoExclamations = 19,
        Paragraph = 21,
        
        DegreePoint = 248,
        TripleLine  = 240,

        CheckerRare = 176,
        CheckerMedium = 177,
        CheckerDense = 178,
        CheckerFull = 219,
        
        SquareFilled = 254,        
        SquareEmpty  = 255,
        
        SmileyFull = 2, 
        SmileyEmpty = 1,

        NullAndVoid = 237,
        
    }
}
