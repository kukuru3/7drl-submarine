﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skulls.Logic.Commands {

    public enum Signal {
        NoCommand,
        KeepHerSteady,
        ChangeHeading,
        ChangeSpeed,
        ChangeDepth,
        Max,
    }
}
