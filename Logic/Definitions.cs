﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ur.Geometry;

namespace Skulls.Logic {
    public class Definitions {
        
        public int SolipsistRadius;

        public int   DefaultTicksPerCommand;
        public float SecondsBetweenSimTicks;
        
        public GeneratorDefs GeneratorDefinition;
        public Velocity[] Velocities;

        private Dictionary<string, Data.Blueprint> blueprints;
        
        public void AddBlueprint(Data.Blueprint blueprint) {
            if (blueprints == null) blueprints = new Dictionary<string, Data.Blueprint>();                    
            if (blueprints.ContainsKey(blueprint.ID)) throw new Exception("Duplicate blueprint : " + blueprint.ID);
            blueprints[blueprint.ID] = blueprint;
        }

        public Data.Blueprint GetBlueprint(string id) {
            Data.Blueprint bp;
            blueprints.TryGetValue(id, out bp);
            return bp;
        }

        public Velocity GetVelocityDefinition(string name) {
            return Velocities.First(v => v.name == name);
        }
                
        /// <summary> After all definitions have been hardcoded / loaded, call this </summary>
        public void Finish() {
        }

        public class GeneratorDefs {

            public int WorldSectorsWide;
            public int WorldSectorsTall;
            public int TilesPerSector;

            public NoiseDef[] WorldgenNoises;
            
            public class NoiseDef {
                public float Persistence;
                public float Scale;
                public int   Octaves;
                public int   Period;
                public float Weight;
            }

            public float Threshold_Deeps;
            public float Threshold_Shallows;
            public float Threshold_Land;            
        }
        
        public class Velocity {
            public string name;
            public float  velocity;
            public string[] status_effects;
        }

        public void AddKeybinding(RLNET.RLKey key, string command) {
            keybindings.Add(key, command);
        }

        public string GetKeybinding(RLNET.RLKey key) {
            var str = "";
            keybindings.TryGetValue(key, out str);
            return str;
        }
        private Dictionary<RLNET.RLKey, string> keybindings = new Dictionary<RLNET.RLKey, string>();
        
    }
}
