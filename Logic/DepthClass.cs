﻿using System;
using System.Collections.Generic;

namespace Skulls.Logic {
    public enum DepthClass {
        None,
        Air,
        Land,
        Surface,
        Shallow,
        Deep,
        MaxDepth,        
    }
}
