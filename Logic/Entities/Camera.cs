﻿using System;
using System.Collections.Generic;

namespace Skulls.Logic.Entities {

    /// <summary> Also acts like "solipsist"</summary>
    public class Camera : MapEntity {
        
        public void AttachTo(MapEntity target) {
            var tracker = new Extensors.Tracker();
            AddExtensor( tracker );
            tracker.Track(target, true);
        }

        bool isSolipsist;
        public void ActivateAsSolipsist() {
            if (isSolipsist) return;
            ChangedSector += WhenIChangeSector;            
            isSolipsist = true;
        }

        private void WhenIChangeSector(MapEntity myself, Map.Sector oldSector, Map.Sector newSector) {
            Manager.Context.Generator.SectorManager.ReferenceSectorCoordinates = newSector.Index;
        }
    }

}
