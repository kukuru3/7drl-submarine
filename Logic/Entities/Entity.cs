﻿using System;
using System.Collections.Generic;
using System.Linq;
using Segmented.DataModel;
using Skulls.Logic.Extensors;

namespace Skulls.Logic.Entities {

    /// <summary> ALL WORLD ENTITIES COME FROM THIS CLASS</summary>
    public abstract class Entity {

        private List<Extensor> extensors;

        /// <summary> Dead. Marked for next cleanup. Can be a result of destruction or world offload </summary>
        public bool Nonexistent { get; private set; }

        public EntityManager Manager { get; internal set; }

        public Entity() {
            extensors = new List<Extensor>();
        }
        
        protected virtual void SimTickLogic() { }
        protected virtual void WhenRegistered() { }

        /// <summary> Called by manager </summary>
        internal void SimTick() {
            foreach (var extensor in extensors.OrderBy(e => e.ExecutionPriority)) extensor.SimTick();
            SimTickLogic();
        }
        /// <summary> Called  by Manager. </summary>
        internal void Registered() {
            WhenRegistered();
        }
        #region Extensors
        public void AddExtensor(Extensor e) {
            e.MyEntity = this;
            extensors.Add(e);
            e.AddedToEntity();
        }

        public void RemoveExtensor(Extensor e) {
            if (extensors.Remove(e)) e.RemovedByEntity();
        }

        public T GetExtensor<T>() where T : Extensor {
            return extensors.OfType<T>().FirstOrDefault();
        } 
        #endregion

        #region Poofing and unloading
        /// <summary> The only proper way to destroy an entity is to call this </summary>
        public void PoofOutOfExistence() {
            if (Nonexistent) return;
            OnPoof?.Invoke();
            Nonexistent = true;
            BeforePoof();
        }

        public event Action OnPoof;

        /// <summary> Will be called once before entity offload to data</summary>
        public virtual void WhenOffloading() { }

        /// <summary> Will be called once after it becomes nonexistent </summary>
        protected virtual void BeforePoof() { }

        internal virtual void InjectValue(string id, Node value) { }
        #endregion
    }
}
