﻿using System;
using System.Collections.Generic;
using System.Linq;
using Skulls.Logic.Map;
using Ur.Grid;
namespace Skulls.Logic.Entities {
    public class MapEntity : Entity {

        

        #region Map affiliations
        private Sector inSector;
        private Coords location;

        public Sector InSector
        {
            get { return inSector; }
            set
            {
                if (inSector == value) return;
                var old = inSector;
                inSector = value;
                old?.LoseEntity(this);
                value?.GainEntity(this);
                ChangedSector?.Invoke(this, old, value);
            }
        }

        public event Action<MapEntity, Sector, Sector> ChangedSector;
        public event Action<MapEntity, Coords, Coords> ChangedLocation;
        public event Action<MapEntity, DepthClass, DepthClass> ChangedDepth;
        public Coords Location
        {
            get { return location; }
            set
            {
                if (location == value) return;
                var old = location;
                location = value;
                InSector = Manager.Context.Map.GetSectorAtTile(location);
                ChangedLocation?.Invoke(this, old, value);
            }
        }

        #endregion

        protected override void BeforePoof() {
            base.BeforePoof();
            Location = new Coords(-1, -1); // will also change sector and purge it from the sector list
        }

        private DepthClass depth;
        public DepthClass Depth { get { return depth; }
            set {
                if (depth == value) return;
                var old = depth;
                depth = value;
                ChangedDepth?.Invoke(this, old, depth);
        }}
        
    }
}
