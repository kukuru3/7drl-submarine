﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Extensors;

namespace Skulls.Logic.Entities {
    public class Vessel : MapEntity {
        
        public string Name { get; set; }

        private StatsResolver      stats;
        private InertialMobility   mobility;

        public StatsResolver Stats { get {
            if (stats == null) stats = GetExtensor<StatsResolver>();
            return stats;
        } }
        

        public InertialMobility Mobility { get {
            if (mobility == null) mobility = GetExtensor<InertialMobility>();
            return mobility;
        } }
        
    }
}
