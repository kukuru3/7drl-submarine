﻿using System;
using System.Collections.Generic;
using System.Linq;
using Skulls.Logic.Entities;
using Ur.Collections;

namespace Skulls.Logic {

    /// <summary> Processes entities and entity references </summary>

    public class EntityManager {

        internal Context Context { get; }

        private List<Entity> entities;
        private Multidict<Entity, EntityReference> references;


        public IReadOnlyList<Entity> Entities { get { return entities;} }

        public EntityManager(Context c) {

            Context = c;
            entities = new List<Entity>();
            references = new Multidict<Entity, EntityReference>();

        }

        public void RegisterEntity(Entity e) {
            e.Manager = this;
            entities.Add(e);
            e.Registered();
        }

        
        /// <summary> Call at least once </summary>
        public void Cleanup() {

            foreach (var e in entities.Where(e => e.Nonexistent)) {

                var allRefs = references.GetAll(e);
                foreach (var @ref in allRefs) @ref.Entity = null;

            }
            entities.RemoveAll(e => e.Nonexistent);

        }

        public EntityReference ObtainReference(Entity e) {
            var @ref = new EntityReference(this);
            @ref.Entity = e;
            references.Add(e, @ref);
            return @ref;
        }

        internal void ReleaseReference(EntityReference @ref) {
            @ref.Entity = null;
        }
        
        internal void ReferenceChanged(EntityReference @ref,  Entity previouslyRefd, Entity newlyRefd) {    
            if (previouslyRefd != null)  
                references.RemoveExact(previouslyRefd, @ref );
            if (newlyRefd != null)
                references.Add(newlyRefd, @ref );
        }
        
    }
}
