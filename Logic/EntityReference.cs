﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Entities;

namespace Skulls.Logic {

    public class EntityReference {

        private Entity currentlyPointsTo;
        private readonly EntityManager em;

        public Entity Entity
        {
            get { return currentlyPointsTo; }
            set {
                if (currentlyPointsTo == value) return;
                var prev = currentlyPointsTo;
                em.ReferenceChanged(this, prev, value);                
                currentlyPointsTo = value;
                Changed?.Invoke(prev, currentlyPointsTo);
            }
        }

        public event Action<Entity, Entity> Changed;
        
        internal EntityReference(EntityManager mgr) {            
            em = mgr;
        }

    }
}
