﻿using System;
using System.Collections.Generic;
using Skulls.Pipeline.ControlPanel;
using System.Linq;
using Skulls.Logic.Entities;

namespace Skulls.Logic.Extensors {
    /// <summary> This actor hooks into the Cockpit UI to generate actions
    /// and it also determins whether or not it is currently blocking
    /// Furthermore, it can also read keyboard input. </summary>
    public class CockpitBasedBlockingActor : Actor {

        Cockpit Cockpit { get; }

        public CockpitBasedBlockingActor(Cockpit cockpit, Pipeline.KeyCommandGenerator commandGenerator ) {
            Cockpit = cockpit;
            Cockpit.SignalGenerated += HandleCockpitSignal;
            commandGenerator.SignalGenerated += HandleKeyboardSignal;
        }

        private void HandleKeyboardSignal(string signalID, params object[] @params ) {
            switch(signalID) {
                case "SpeedUp":         ChangeVelocityClassBy(1); EndPlayerTurn(); break;
                case "SpeedDown":       ChangeVelocityClassBy(-1); EndPlayerTurn(); break;
                case "Left":            TryChangeHeading(-1); EndPlayerTurn(); break;
                case "Right":           TryChangeHeading( 1); EndPlayerTurn(); break;
                case "Rise":            TryChangeDepth(-1);  break;
                case "Dive":            TryChangeDepth(1); break;
                case "KeepHerSteady":   EndPlayerTurn(); break;
            }
        }

        private void TryChangeHeading(int delta) {
            var m = MyEntity.GetExtensor<InertialMobility>();            
            m.ChangeHeading(delta);            
        }

        private void HandleCockpitSignal(ControlPanelEffector sender, string signalID, params object[] @params) {
            switch(signalID) {
                case "SpeedLever.Position":
                    var z = (int)@params[0];
                    ChangeVelocityClassTo(z);
                    EndPlayerTurn();
                    break;
                case "DepthLever.Position":
                    var delta = (int)@params[0];
                    TryChangeDepth(delta);
                    ((Lever)sender).SetValueWithoutEvents(0);
                    break;
                
            }
        }

        private void TryChangeDepth(int delta) {
            if (delta == 0) return;

            var dt = MyEntity.GetExtensor<DiveTanks>();
            if (dt.CanIChangeDepthTo(((Vessel)MyEntity).Depth + delta )) {
                if (delta == -1) dt.Rise();
                if (delta == 1) dt.TryDive();
                EndPlayerTurn();
            } else {
               // cannot dive - generate message  
            }
        }
        private void ChangeVelocityClassBy(int z) {
            //MyEntity.GetExtensor<InertialMobility>().ChangeVelocityClassBy(z);
        }
        private void ChangeVelocityClassTo(int z) {
            //MyEntity.GetExtensor<InertialMobility>().ChangeVelocityClassTo(z);                        
        }

        int turnsLeftBlocking = 0;
        
        void EndPlayerTurn() {            
            turnsLeftBlocking = MyEntity.Manager.Context.Definitions.DefaultTicksPerCommand;
            MyEntity.Manager.Context.Simulator.SimulationActive = true;
        }

        internal override void SimTick() {
            
            turnsLeftBlocking --;            
            //MyEntity.Manager.Context.Simulator.SimulationActive = turnsLeftBlocking > 0;
            base.SimTick();
        }

    }
}
