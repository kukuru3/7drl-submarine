﻿using System;
using System.Collections.Generic;
using Skulls.Pipeline.ControlPanel;
using System.Linq;


namespace Skulls.Logic.Extensors {
    class CockpitIndicatorController : Extensor {

        Cockpit Cockpit { get; }
        
        public CockpitIndicatorController(Cockpit cockpit) {
            Cockpit = cockpit;    
        }

        internal override void SimTick() {
            base.SimTick();
            var mobility = MyEntity.GetExtensor<InertialMobility>();

            //var sl = (GetEffectorBySignalID("SpeedLever") as Lever);            
            //if (sl.LeverValue != mobility.SpeedClass ) sl.SetValueWithoutEvents(mobility.SpeedClass);

            var si = GetIndicatorBySignalID("SpeedIndicator");

            //si.SetIndicatorValueExternally((float)mobility.SpeedClass / (mobility.MyVessel.Stats.Blueprint.Specs.Velocities.Length-1) );
                        
            var gi = GetIndicatorBySignalID("DepthIndicator");
            var depthNeedleOffset = 0f;
            switch(mobility.MyVessel.Depth) {
                case DepthClass.Surface: depthNeedleOffset = 0f; break;
                case DepthClass.Shallow: depthNeedleOffset = 0.1f; break;
                case DepthClass.Deep: depthNeedleOffset = 0.6f; break;
                case DepthClass.MaxDepth: depthNeedleOffset = 1f; break;
            }
            gi.SetIndicatorValueExternally(depthNeedleOffset);

        }

        ControlPanelEffector GetEffectorBySignalID(string signalID) {
            return Cockpit.FlattenHierarchy().OfType<ControlPanelEffector>().First(el => el.SignalIdentity == signalID);
        }

        ControlPanelIndicator GetIndicatorBySignalID(string indicatorID) {
            return Cockpit.FlattenHierarchy().OfType<ControlPanelIndicator>().First(el=> el.IndicatorIdentity == indicatorID);
        }

    }
}
