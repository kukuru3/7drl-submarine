﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Entities;

namespace Skulls.Logic.Extensors {
    public class DiveTanks : Extensor {

        Vessel MyVessel { get {  return MyEntity as Vessel; } }

        int DesiredDelta { get; set; }

        public void TryDive() {
            DesiredDelta = 1;
        }

        public void Rise() {
            DesiredDelta = -1;
        }

        public bool CanIChangeDepthTo(DepthClass d) {
            var loc = MyVessel.Location;
            // can't dive below seabed floor:
            if (MyEntity.Manager.Context.Map.GetTileAtWorldspace( loc.X, loc.Y).Depth < d) return false;
            // can't rise above surface:
            if (d < DepthClass.Surface) return false;
            return true;
        }

        internal override void SimTick() {
            base.SimTick();
            if (DesiredDelta != 0) {
                var d = MyVessel.Depth + DesiredDelta;
                if (CanIChangeDepthTo(d)) MyVessel.Depth = d;                
                DesiredDelta = 0;
            }            
        }

    }
}
