﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Entities;

namespace Skulls.Logic.Extensors {
    public abstract class Extensor {

        public Entity MyEntity { get; internal set; }
        public int    ExecutionPriority { get; internal set; }

        internal virtual void SimTick() { }
        
        internal void RemovedByEntity() {            
            OnPoof();
            MyEntity = null;
        }

        internal virtual void AddedToEntity() { }

        protected virtual void OnPoof() { }
    }
}
