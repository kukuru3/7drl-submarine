﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Entities;
using System.Linq;
using Ur.Grid;
using Ur;
using Ur.Geometry;

namespace Skulls.Logic.Extensors {

    /// <summary> Intended to replace the current mobility clusterfucks </summary>
    public class InertialMobility : Extensor {

        public Vessel MyVessel { get { return MyEntity as Vessel; } }
        
        /// <summary> Degrees </summary>
        public int Heading { get; private set; }
                
        public float CurrentVelocity { get; private set; }
        
        /// <summary> Just to be clear, this is for horizontal purposes only, it has nothing to do with dive / raise</summary>
        public bool CanMoveIntoTileWithDepth(DepthClass depth) {
            return depth >= MyVessel.Depth;
        }

        private Vector2 finePosition;

        public Vector2 WorldPosition {
            get { return finePosition; }
            set {
                finePosition = value;
                UpdateLocation();
            }
        }
        
        internal void ChangeHeading(int delta) {
            var maneuver = MyVessel.Stats.GetFinalManeuver();
            Heading += maneuver * delta;
            Heading = Heading.Wrap(360);
        }
        
        internal override void AddedToEntity() {
            base.AddedToEntity();
            MyVessel.ChangedLocation += VesselWasMoved;
            PositionAtCenterOfTile(MyVessel.Location); LastLegalLocation = MyVessel.Location;
        }

        private void VesselWasMoved(MapEntity entity, Coords oldLoc, Coords newLoc) {
            if (!_locationGuard) PositionAtCenterOfTile(newLoc);
        }

        protected override void OnPoof() {
            MyVessel.ChangedLocation -= VesselWasMoved;
        }

        bool _locationGuard;
        private void UpdateLocation() {
            _locationGuard = true;
            MyVessel.Location = WorldPosition.Truncate();
            _locationGuard = false;
        }

        internal override void SimTick() {
            base.SimTick();
            var finalSpeedVector = Vector2.FromPolar(Heading, CurrentVelocity);
            TryMove(finalSpeedVector);
        }

        private Coords LastLegalLocation = new Coords(-1, -1);

        private void TryMove(Vector2 delta) {
            
            var A = WorldPosition;
            var B = WorldPosition + delta;

            var map = MyEntity.Manager.Context.Map;
            var collision = PoorMansPhysics.TryTraverseGrid(A, B, delegate(Coords c) {
                var wt = map.GetTileAtWorldspace(c.X, c.Y);
                if (wt == null) return true;
                if (!CanMoveIntoTileWithDepth(wt.Depth)) return true;
                return false;
            });

            if (collision == null) {
                WorldPosition = B;
                LastLegalLocation = MyVessel.Location;
            } else {
                //WorldPosition = A.Lerp(collision.CollisionPoint, 0.9f);
                Collided(collision);                
            }
        }
        
        void PositionAtCenterOfTile(Coords tile) {
            WorldPosition = Vector2.One * 0.5f + new Vector2(tile.X, tile.Y);
        }

        void Collided(PoorMansPhysics.Collision collision) {
            /// <summary> Sets location to the center of the last known "legal" tile </summary>
            PositionAtCenterOfTile(LastLegalLocation);            
            Debug.Log("Collided with terrain");
        }
    }
}
     