﻿using System;
using System.Collections.Generic;
using Alcove.Light;
using Ur.Grid;

namespace Skulls.Logic.Extensors {
    public class LogicalLight : Extensor {

        Alcove.Light.Defaults.SimpleLightSource MyLightSource;

        public LogicalLight() {
            
        }
        

        internal override void AddedToEntity() {
            MyLightSource = new Alcove.Light.Defaults.SimpleLightSource();
            MyLightSource.Radius = 20;
            MyLightSource.Intensity = 1f;            
            MyEntity.Manager.Context.Map.Lightmap.RegisterLightsource(MyLightSource);
            (MyEntity as Entities.MapEntity).ChangedLocation += MyEntityMoved;
        }

        private void MyEntityMoved(Entities.MapEntity e, Coords oldCrds, Coords newCrds) {
            MyLightSource.Position = newCrds;            
        }

        protected override void OnPoof() {
            MyEntity.Manager.Context.Map.Lightmap.UnregisterLightsource(MyLightSource);
            (MyEntity as Entities.MapEntity).ChangedLocation -= MyEntityMoved;
            MyLightSource = null;
            base.OnPoof();
        }



    }
}
