﻿using System;
using System.Collections.Generic;
using System.Linq;
using Skulls.Logic.Entities;
using RLNET;

namespace Skulls.Logic.Extensors {
    public class MapRenderInfo : Extensor {

        public string   Glyph { get; set; }
        public int      Char  { get; private set; }
        public RLColor  Color { get; set; }

        public MapRenderInfo() { }

        internal override void AddedToEntity() {
            base.AddedToEntity();
            Char =  Glyph[0];
        }

        public void Render(int sx, int sy) {
            MyEntity.Manager.Context.Renderer.Console.SetChar(sx, sy, Char);
            MyEntity.Manager.Context.Renderer.Console.SetColor(sx, sy, Color);
        }

    }
}
