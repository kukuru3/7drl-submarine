﻿using System;
using System.Collections.Generic;

using Skulls.Logic.Entities;
using Alcove.FOV;
using Ur.Grid;

namespace Skulls.Logic.Extensors {
    public class PlayerEyes : Extensor {

        public new MapEntity MyEntity { get { return base.MyEntity as MapEntity; } }

        public IEnumerable<Coords> VisibleTiles { get { return visible; } }

        public int Radius = 28;        
        bool dirty = false;
        HashSet<Coords> visible = new HashSet<Coords>();

        // optimizations
        private Map.WorldMap world; 
        private DepthClass   effectiveDepth;

        internal override void AddedToEntity() {
            MyEntity.ChangedLocation += EntityMoved;
            MyEntity.ChangedDepth += EntityChangedDepth;            
        }
        
        bool DoesTileObstructLOS(int wx, int wy) {
            
            var tileD = world.GetTileAtWorldspace(wx, wy)?.Depth ?? DepthClass.Air;

            if (tileD < effectiveDepth && effectiveDepth >= DepthClass.Shallow) return true;
            return false;

        }        

        internal override void SimTick() {
            if (dirty) DoUpdate();                
        }

        void DoUpdate() {
            world = MyEntity.Manager.Context.Generator.World;
            effectiveDepth = MyEntity.Depth;
            visible.Clear();
            visible.UnionWith(
                FieldOfView.Compute(
                    FieldOfView.Algorithm.RecursiveShadowcast,                
                    MyEntity.Manager.Context.Map.Lightmap,
                    MyEntity.Location,
                    Radius,
                    DoesTileObstructLOS
                )
            );

            dirty = false;
        }
        
        protected override void OnPoof() {
            base.OnPoof();
            MyEntity.ChangedLocation -= EntityMoved;
            MyEntity.ChangedDepth    -= EntityChangedDepth;
        }

        private void EntityChangedDepth(MapEntity arg1, DepthClass arg2, DepthClass arg3) {
            MarkForUpdate();
        }


        private void MarkForUpdate() {
            dirty = true;
            DoUpdate(); 
        }

        private void EntityMoved(MapEntity arg1, Coords arg2, Coords arg3) {
            MarkForUpdate();
        }
        
    }
}
