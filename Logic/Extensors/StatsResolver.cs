﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Entities;

namespace Skulls.Logic.Extensors {
    /// <summary> Serves to "hook into" definition blueprints and pull stats on demand</summary>
    public class StatsResolver : Extensor {

        private const int MANEUVER_MULTIPLIER = 3;

        public Data.Blueprint Blueprint { get; }

        public StatsResolver(Data.Blueprint bp) {
            Blueprint = bp;
        }

        internal int GetFinalManeuver() {
            UpdateStatus();
            return Blueprint.Specs.Maneuver * MANEUVER_MULTIPLIER;
        }

        internal float GetFinalVelocity(int velocityClass) {
            UpdateStatus();
            return Blueprint.Specs.Velocities[velocityClass].velocity;
        }


        #region Status Effects

        void UpdateStatus() {

        }

        bool _recalcStatus = false;
        void StatusNeedsRecalc() {
            _recalcStatus = true;
        }
        private HashSet<string> activeStatusEffects = new HashSet<string>();
        
        internal void RemoveStatus(string se) { activeStatusEffects.Remove(se); StatusNeedsRecalc(); }
        internal void AddStatus(string se) {  activeStatusEffects.Add(se);      StatusNeedsRecalc(); }

        internal void RemoveStatuses(IEnumerable<string> statusEffects) {
            foreach (var se in statusEffects) RemoveStatus(se);
        }

        internal void AddStatuses(IEnumerable<string> statusEffects) {
            foreach (var se in statusEffects) AddStatus(se);
        } 
        #endregion
    }
}
