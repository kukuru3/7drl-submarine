﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Entities;

namespace Skulls.Logic.Extensors {
    public class Tracker : Extensor {
        
        EntityReference tracked;

        private new MapEntity MyEntity { get { return base.MyEntity as MapEntity; } }
        public MapEntity TracksWhat { get {
            return tracked?.Entity as MapEntity;
        } }

        private bool MatchDepthAsWell { get; set; }
        
        private Ur.Grid.Coords offset;
        public Ur.Grid.Coords Offset
        {
            get { return offset; }
            set { if (offset == value) return; offset = value; UpdateEntityLocation(); }
        }


        void UpdateEntityLocation() {
            var tme = (tracked?.Entity as MapEntity);
            if (tme != null) { 
                MyEntity.Location = tme.Location + Offset;
                if (MatchDepthAsWell) MyEntity.Depth = tme.Depth;
            }
        }

        public void Track(MapEntity target, bool alsoMatchDepth) {
            MatchDepthAsWell = alsoMatchDepth;
            tracked = MyEntity.Manager.ObtainReference(target);
            UpdateEntityLocation();

            var trackedEntity = ((MapEntity)tracked.Entity);
            trackedEntity.ChangedLocation += TrackedEntityMoved;
            trackedEntity.ChangedDepth += TrackedEntityChangedDepth;

            tracked.Changed += delegate(Entity a, Entity b) {
                Unsubscribe(a);
            };

        }

        void Unsubscribe(Entity e) {
            ((MapEntity)e).ChangedLocation -= TrackedEntityMoved;            
            ((MapEntity)e).ChangedDepth    -= TrackedEntityChangedDepth;
        }

        private void TrackedEntityChangedDepth(MapEntity entity, DepthClass oldDepth, DepthClass newDepth) {
            if (MatchDepthAsWell) UpdateEntityLocation();
        }

        private void TrackedEntityMoved(MapEntity entity, Ur.Grid.Coords old, Ur.Grid.Coords newCoords) {
            UpdateEntityLocation();
        }
        
        protected override void OnPoof() {
            if (tracked != null) { 
                tracked.Entity = null;
                tracked = null;
            }
        }

    }
}
