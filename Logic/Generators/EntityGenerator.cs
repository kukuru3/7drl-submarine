﻿using System;
using System.Collections.Generic;
using Skulls.Data;
using Skulls.Logic.Entities;
using Skulls.Logic.Extensors;
using Segmented.DataModel;

namespace Skulls.Logic.Generators {
    /// <summary> Generates entities from blueprints </summary>
    public class EntityGenerator{
        public Context Context { get; }

        public EntityGenerator(Context context) {
            Context = context;
        }

        /// <summary> Generates and registers the entity </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entity Generate(string id) {
            
            var bp = Context.Definitions.GetBlueprint(id);
            if (bp == null) {
                Debug.Log("Cannot spawn '" + id + " - blueprint not found");
                return null;
            }

            Entity e = null;
                        
            switch (bp.Class) {
                case Blueprint.Classes.Vessel:
                    e = new Vessel();
                    e.AddExtensor(new InertialMobility());
                    break;
                case Blueprint.Classes.Creature:
                    Debug.Log("Cannot spawn : creatures not implemented");
                    break;
                default:
                    Debug.Log("Cannot spawn '" + id + " - class not specified");
                    return null;
            }

            if (e == null) return null;

            Context.Entities.RegisterEntity(e);

            var bpref = new StatsResolver(bp);
            e.AddExtensor(bpref);
            
            InjectComponents(e, bpref);
            return e;
        }

        private void InjectComponents(Entity e, StatsResolver bpref) {
            var vi = new Segmented.DataModel.RecursiveValueInjector();

            var rootNode = bpref.Blueprint.ComponentsNode;
            foreach (var child in rootNode.Children) {
                // component type comes from id
                var componentTypeID = child.Get("type").Value;
                var paramNode         = child.Get("params");
                var g = nameof(Skulls);
                var type = Type.GetType(g + ".Logic.Extensors." + componentTypeID );
                if (type != null) {
                    var component = vi.Inject(type, paramNode) as Extensor;
                    e.AddExtensor(component); 
                    if (paramNode != null) {
                        foreach (var propChild in paramNode.Children) {
                            e.InjectValue(propChild.ID, propChild);
                        }
                    }
                }                
            }
        }
    }
}
