﻿using System;
using System.Collections.Generic;
using Ur.Random;
using Ur.Grid;
using Ur;

namespace Skulls.Logic.Generators {

    /// <summary> Responsible for all the terrain stuff in the game </summary>
    public class SeabedGenerator {

        Noise[] noises;

        private WorldGenerator WorldGenerator { get; }
        
        public SeabedGenerator(WorldGenerator worldGenerator) {
            WorldGenerator = worldGenerator;            
        }

        public DepthClass GetFinalDepthAt(int worldX, int worldY) {
            var sum = GetNoiseSumAt(worldX, worldY);
            return DepthFromRaw(sum);            
        }

        DepthClass DepthFromRaw(float raw) {
            var g = WorldGenerator.World.Context.Definitions.GeneratorDefinition;
            if (raw < g.Threshold_Deeps )   return DepthClass.MaxDepth;
            if (raw < g.Threshold_Shallows) return DepthClass.Deep;
            if (raw < g.Threshold_Land)     return DepthClass.Shallow;

            return DepthClass.Land;
        }

        internal float GetNoiseSumAt(int x, int y ) {            
            var totalWeight = 0f;
            var result      = 0f;
            for (var i = 0; i < noises.Length; i++) {
                var n = GetNoiseAt(x, y, i);
                float w = noises[i].definition.Weight;
                totalWeight += w;
                result      += w * n;
            }
            return result / totalWeight;
        }

        internal float GetNoiseAt(int wx, int wy, int index) { 
            var n = noises[index];
            float x = (float)(wx) / n.definition.Period;
            float y = (float)(wy) / n.definition.Period;
            return n.perlin[x, y];
        }
        
        public void RegenerateNoises() {
            var rng = new Generator(WorldGenerator.World.RNGSeed);
            var nl = new List<Noise>();
            foreach (var noisedef in WorldGenerator.World.Context.Definitions.GeneratorDefinition.WorldgenNoises) {
                var n = new Noise();
                n.definition = noisedef;
                n.perlin = new PerlinNoise(noisedef.Persistence, noisedef.Octaves, rng);
                nl.Add(n);
            }
            noises = nl.ToArray();
        }
        
        private int MapDepth(Map.SectorTypes type) {

            switch (type) {
                case Map.SectorTypes.Undetermined:      return -999;
                case Map.SectorTypes.Land:              return -1;
                case Map.SectorTypes.Shallow:           return 1;
                case Map.SectorTypes.Medium:            return 2;
                case Map.SectorTypes.Deep:              return 3;
            }
            return -999;
        }

        private class Noise {
            public Definitions.GeneratorDefs.NoiseDef definition;
            public PerlinNoise                        perlin;
        }

        public DepthClass[,] GenerateTilemap(Map.Sector s) {

            var rng = new Generator(s.SectorSeed);
            var result = new DepthClass[s.TilesPerSector, s.TilesPerSector];            
            for (var y = 0; y < s.TilesPerSector; y++)
            for (var x = 0; x < s.TilesPerSector; x++) {
                var wx = s.Offset.X + x; var wy = s.Offset.Y + y;                
                result[x,y] = GetFinalDepthAt(wx, wy);
            }
            return result;
        }
        
    }
}


//CoarsePerlin = new PerlinNoise(gendef.SectorMapPerlin.Persistence, gendef.SectorMapPerlin.Octaves, DeterministicRNG);

//var sectorMap = CoarsePerlin.ExtractMap(World.SectorsWide, World.SectorsTall, gendef.SectorMapPerlin.Scale);
            
//foreach (var i in sectorMap.Iterate()) {
//    var sector = World.GetSectorByIndex(i.X, i.Y);
//    if      (i.Value < gendef.Threshold_Deeps)      sector.Type = SectorTypes.Deep;
//    else if (i.Value < gendef.Threshold_Shallows)   sector.Type = SectorTypes.Medium;
//    else if (i.Value < gendef.Threshold_Land)       sector.Type = SectorTypes.Shallow;
//    else                                            sector.Type = SectorTypes.Land;
//}

            //void GenerateFineGrainedCheatPerlin() {
        //    var gendef = Context.Definitions.GeneratorDefinition;

        //    finegrainRepeatFactor = 1f / gendef.FinegrainRepeat;

        //    FinegrainedPerlin = new PerlinNoise(
        //        gendef.FinegrainedMapPerlin.Persistence, 
        //        gendef.FinegrainedMapPerlin.Octaves,
        //        DeterministicRNG
        //    );

        //}

        //public int GetDepthAt(int worldX, int worldY) {
        //    var gendef = Context.Definitions.GeneratorDefinition;
        //    var scaleValue = gendef.SectorMapPerlin.Scale;

        //    float coarseX = scaleValue * worldX / World.SectorsWide / World.SectorsWide;
        //    float coarseY = scaleValue * worldY / World.SectorsTall / World.SectorsTall;
        //    var sum = CoarsePerlin[coarseX, coarseY] + GetFinegrainedPerlinAt(worldX, worldY);
        //    sum -= 0.8f;

        //    if      (sum < gendef.Threshold_Deeps)      return 3;
        //    else if (sum < gendef.Threshold_Shallows)   return 2;
        //    else if (sum < gendef.Threshold_Land)       return 1;
        //    else                                        return -1;

        //}

        //float GetFinegrainedPerlinAt(int worldX, int worldY) {

        //    float x = finegrainRepeatFactor * worldX;
        //    float y = finegrainRepeatFactor * worldY;            
        //    return FinegrainedPerlin[x, y];

        //}