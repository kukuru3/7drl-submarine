﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Map;
using System.Linq;
using Ur.Grid;
using Ur;
using Alcove;


namespace Skulls.Logic.Generators {

    /// <summary> Manages offloading and reloading sectors </summary>
    public class SectorManager : Alcove.IDynamicWorld {

        public WorldGenerator Generator { get; }        
        public WorldMap Map { get; internal set; }
        

        bool    enqueueRefFrameChange;
        private Coords refSec;
        
        public Coords ReferenceSectorCoordinates
        {
            get { return refSec; }
            set { if (refSec == value) return; refSec = value; enqueueRefFrameChange = true; }
        }

        private HashSet<Sector> simulatedSectors;
        
        /// <summary> Call after every sim tick </summary>
        internal void UpdateReferenceFrame(bool force = false) {
            if (!enqueueRefFrameChange && !force) return;
            enqueueRefFrameChange = false;
            var o = Map.Context.Definitions.SolipsistRadius;
            var sector = Map.GetSectorByIndex(ReferenceSectorCoordinates.X, ReferenceSectorCoordinates.Y);            
            var desiredSectors = new HashSet<Sector>();
            for (var y = sector.Index.Y - o; y <= sector.Index.Y + o; y++) 
            for (var x = sector.Index.X - o; x <= sector.Index.X + o; x++) {
                var sec = Map.GetSectorByIndex(x, y);
                    if (sec != null) desiredSectors.Add(sec);
            }
            simulatedSectors.ExceptWith(desiredSectors);
            // simulated sectors now only contains the ones that will be removed
            foreach (var s in simulatedSectors) Offload(s);
            simulatedSectors.Clear();
            simulatedSectors.UnionWith(desiredSectors);
            foreach (var s in simulatedSectors) Activate(s);            
        }

        void Offload(Sector s) {
            s.State = Sector.States.Unloaded;
        }

        void Activate(Sector s) {
            s.State = Sector.States.Simulated;
        }

        public SectorManager(WorldGenerator worldGenerator) {
            Generator = worldGenerator;
            simulatedSectors = new HashSet<Sector>();   
            refSec = new Coords(-999, -999); // to force update the first time you set it
        }

        internal void SectorIsSimulated(Sector sector, SectorSimulation details) {

            // generate sector tiles from local generator
            Debug.Log("Generating sector " + sector.Index.X + "," + sector.Index.Y);
            
            var tilemap = Generator.SeabedGenerator.GenerateTilemap(sector);
            var n = Generator.World.Context.Definitions.GeneratorDefinition.WorldgenNoises.Length ;
            foreach (var i in tilemap.Iterate()) {
                var tile = details.GetTile(i.X, i.Y);
                tile.Depth = i.Value;
                // debug:
                tile.NoiseValues = new float[n];
                for (var g = 0; g < n; g++) tile.NoiseValues[g] = Generator.SeabedGenerator.GetNoiseAt(tile.WorldCoords.X, tile.WorldCoords.Y, g);
                tile.NoiseSum = Generator.SeabedGenerator.GetNoiseSumAt(tile.WorldCoords.X, tile.WorldCoords.Y);
                tile.RawElevation = tile.NoiseSum;
            }

            NotifySystemsOfNewTiles(details.GetAllTiles());
            

            // fill entities from disk
        }

        internal IEnumerable<Sector> EnumerateSimulatedSectors() {
            return simulatedSectors;
        }

        internal void SectorNoLongerSimulated(Sector sector, SectorSimulation details) {
            Debug.Log("Unloading sector " + sector.Index.X + "," + sector.Index.Y);
            // serialize sector details

            NotifySystemsOfLostTiles(details.GetAllTiles());
            
            var ss = new Serialization.SectorSerializer();

            ss.OffloadSectorDetails(sector);

            foreach (var entity in sector.Entities.ToArray()) {
                entity.WhenOffloading();
                entity.PoofOutOfExistence();
            }
            
        }

        private List<IWorldSystem> systems = new List<IWorldSystem>();
        public event Action<IDynamicWorld, IEnumerable<Coords>> RegionAdded;
        public event Action<IDynamicWorld, IEnumerable<Coords>> RegionRemoved;
        
        public void RegisterSystem(IWorldSystem system) {
            systems.Add(system);
            system.Registered(this);
        }

        internal void NotifySystemsOfNewTiles(IEnumerable<Tile> tiles) {
            RegionAdded?.Invoke(this, tiles.Select(t => t.WorldCoords));
        }

        internal void NotifySystemsOfLostTiles(IEnumerable<Tile> tiles) {
            RegionRemoved?.Invoke(this, tiles.Select(t => t.WorldCoords));
        }

        public IEnumerable<Coords> ListActiveTiles() {
            foreach (var s in EnumerateSimulatedSectors()) {
                foreach (var t in s.Details.GetAllTiles()) yield return t.WorldCoords;
            }
        }

    }
}
