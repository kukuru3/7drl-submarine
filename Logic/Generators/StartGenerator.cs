﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ur.Random;
using Ur.Grid;
using Skulls.Logic.Map;

namespace Skulls.Logic.Generators {
    /// <summary> Generates starting entities </summary>
    public class StartGenerator {

        Context Context { get; }

        public StartGenerator(Context c) {
            Context = c;
        }

        Generator DeterministicRNG;
        public void GenerateAll() {
            
            // again, initialize deterministic RNG (it was only used for perlin, so resetting it should be okay)
            DeterministicRNG = new Generator(Context.Map.RNGSeed);
            
            Tile pickedTile = null;

            while(pickedTile == null) { 
                var startingSector = GetStartingSector();
                Context.Generator.SectorManager.ReferenceSectorCoordinates = startingSector; 
                Context.Generator.SectorManager.UpdateReferenceFrame(); // this should prompt sector generation
                var tiles = Context.Map.GetSectorByIndex(startingSector.X, startingSector.Y).Details.GetAllTiles();
                tiles = tiles.Where(t => t.Depth > DepthClass.Land);
                pickedTile = tiles.PickRandom(DeterministicRNG);                
            }
            
            var vessel = Context.EntityGenerator.Generate("player_ship") as Entities.Vessel;         
            vessel.Location = pickedTile.WorldCoords + new Coords(0,1);
            vessel.Depth    = DepthClass.Surface;
            vessel.Name     = "U-473";
            
            // create relays
                        
            var camera = new Entities.Camera();
            Context.Entities.RegisterEntity(camera);            
            camera.AttachTo(vessel);
            camera.ActivateAsSolipsist();
            camera.AddExtensor(new Extensors.PlayerEyes());

            //var f = Context.Pipeline.RootElement.FlattenHierarchy().OfType<Pipeline.Views.WorldView>().First();
            //f.MyCamera = camera;

            vessel.Location = pickedTile.WorldCoords;
            Context.Generator.SectorManager.UpdateReferenceFrame(true);

            var cockpit = Context.GUI.CreateCockpit();
            var keyListener = Context.GUI.CreateKeyboaardCommandGenerator();
            Context.GUI.CreateASCIIPicker(); // for debug purposes
            
            vessel.AddExtensor( new Extensors.CockpitBasedBlockingActor(cockpit, keyListener));
            vessel.AddExtensor( new Extensors.CockpitIndicatorController(cockpit));
            
            Context.Simulator.WorldSimTick();

        }

        Coords GetStartingSector() {
            // starting sector : somewhere in the left quarter of the map
            var eligible = Context.Map.GetAllSectors().Where(s => s.Index.X < Context.Map.SectorsWide / 4 && s.Type != SectorTypes.Land);
            var source = eligible.PickRandom(DeterministicRNG);
            return source.Index;
        }
    }
}
