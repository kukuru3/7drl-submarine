﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Skulls.Logic.Map;

namespace Skulls.Logic.Generators {

    /// <summary> Creates a new world on disk. Determines seed, generates sectors and stuff like that. </summary>
    public class WorldCreator {

        private HashSet<long> existingWorlds;
        private Ur.Random.Generator idRNG;

        public WorldCreator() {
            idRNG = new Ur.Random.Generator();
        }

        private string WorldFilename(long id) {
            return "world_" + Convert.ToString(id, 16) + ".xml";
        }

        private string WorldFolderName(long id) {
            return Convert.ToString(id, 16);
        }

        private long IDFromFilename(string filename) {
            var dash = filename.LastIndexOf('_');
            var dot  = filename.LastIndexOf('.');
            var worldIDString = filename.Substring(dash+1, dot - dash - 1);
            return Convert.ToInt64(worldIDString, 16);
        }

        private string WorldFileMask() {
            return "world_*.xml";            
        }
        

        public long[] EnumerateExistingWorlds() {
            Debug.Log("Enumerating Existing worlds...");
            existingWorlds = new HashSet<long>();
            var wf = Serialization.FileOps.GetWorldsFolder();
            var dir = new DirectoryInfo(wf);
            if (!dir.Exists) {
                Debug.Log("App folder not found, creating...");
                dir.Create();
                return new long[] { } ;
            }

            //if (!dir.Exists) throw new Errors.WorldSerializationException("Worlds serialization folder does not exist");

            var worldFiles = dir.GetFiles("world_*.xml",SearchOption.TopDirectoryOnly);

            foreach (var file in worldFiles) {                
                var worldID = IDFromFilename(file.Name);
                existingWorlds.Add(worldID);
            }

            Debug.Log(existingWorlds.Count.ToString() + " found" );
            return existingWorlds.ToArray();

            
        }

        
        public void DestroyWorldFiles(long id) {
            Debug.Log("Destroying world files for " + Convert.ToString(id, 16));
            var file = WorldFilename(id);
            var folder = WorldFolderName(id);
            Serialization.FileOps.DestroyFile(file);
            Serialization.FileOps.DestroyFolder(folder);
        }

        /// <summary>For purposes of finding ID</summary>
        
        public long FindUniqueIdentity() {
            if (existingWorlds == null) EnumerateExistingWorlds();
            
            bool found = false;
            int counter = 0;

            while(!found) {                
                long id = (idRNG.Next(0, 255) << 16) | (idRNG.Next(0, int.MaxValue-1));
                if (!existingWorlds.Contains(id)) {
                    found = true; // redundant but oh well
                    return id;
                }
                counter++;
                if (counter >= 500) {
                    throw new Errors.WorldSerializationException("Cannot seem to find a unique id?!");
                }
            }

            return -1;
            
        }

        /// <summary> Loads an existing world from disk </summary>
        public WorldMap CreateWorldFromFile(Context context, long id) {
            
            var map = new Map.WorldMap(context);
            context.AssignWorld(map);

            var ws = new Serialization.WorldDeserializer();
            var filename = WorldFilename(id);
            var doc = Serialization.FileOps.LoadDocument(filename);

            ws.FillWorldFromDocument(map, doc);            

            context.Generator.FillDeterministicInformation();
            context.Generator.World.InitializeLightmap();

            return map;
        }

        /// <summary> Creates a new world and accompanying files  </summary>        
        public Map.WorldMap CreateNewWorld(Context context, int w, int h, int dim) {

            var id = FindUniqueIdentity();
            Debug.Log("Creating unique world with id " + Convert.ToString(id, 16));
            
            var world = new Map.WorldMap(context);
            context.AssignWorld(world);

            world.ConstructSectors(w, h, dim);

            world.Identity = id;
            world.RNGSeed  = new[] {
                idRNG.Next(0, int.MaxValue - 1),
                idRNG.Next(0, int.MaxValue - 1),
                idRNG.Next(0, int.MaxValue - 1),
                idRNG.Next(0, int.MaxValue - 1),
                idRNG.Next(0, int.MaxValue - 1),
                idRNG.Next(0, int.MaxValue - 1),
            };

            context.Generator.FillDeterministicInformation();
            context.Generator.World.InitializeLightmap();
            
            var ws = new Serialization.WorldSerializer();
            var document = ws.SerializeToDoc(world);
            var filename = WorldFilename(id);

            var result = 
                Serialization.FileOps.SaveDocumentToFile(document, "", filename) 
                | 
                Serialization.FileOps.CreateFolder( WorldFolderName(id) );

            if (result) return world;

            return null;
            
        }
        
    }
}
