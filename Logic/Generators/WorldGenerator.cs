﻿using System;
using System.Collections.Generic;
using Ur.Grid;
using Ur.Random;
using Skulls.Logic.Map;
using Ur;
using System.Linq;


namespace Skulls.Logic.Generators {

    /// <summary> World generator deals with dynamic on-the-fly generation of an existing world, NOT with creating a new World instance. </summary>
    public class WorldGenerator {

        Context Context { get; }
        SkullsGame Game { get { return Context.Game; } }

        public SeabedGenerator SeabedGenerator { get; }

        public WorldMap World { get; internal set; }
        
        public WorldGenerator(Context context) {
            Context = context;
            SectorManager = new SectorManager(this);
            SeabedGenerator = new SeabedGenerator(this);
        }

        public SectorManager SectorManager { get; }
        public StartGenerator WorldStarter { get { return new StartGenerator(Context); } }

        Generator DeterministicRNG;
        
        
        public void FillDeterministicInformation() {
            DeterministicRNG = new Generator(World.RNGSeed);
            GenerateWorldName();
            GenerateSectorMap();
            SeabedGenerator.RegenerateNoises();
            DeterministicRNG = null;
        }

        void GenerateWorldName() {
            Context.MnemhotepGenerator.Reset();
            var name = Context.MnemhotepGenerator.Convert("$(world_naming.world_name)", DeterministicRNG);            
            World.Name = name;
        }
        
        void GenerateSectorMap() {
            // create sector seeds, and make all sectors            
            foreach (var sector in World.GetAllSectors()) {
                sector.SectorSeed = new[] {
                    DeterministicRNG.Next(0, int.MaxValue-1),
                    DeterministicRNG.Next(0, int.MaxValue-1),
                    DeterministicRNG.Next(0, int.MaxValue-1),
                };
                sector.Type = SectorTypes.Undetermined; // initial
            }            
        }
        
        

    }
}
