﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Entities;
using Ur.Grid;

namespace Skulls.Logic.Map {

    // all world sectors are referenced in the memory at all times
    public class Sector {
        
        public int TilesPerSector { get { return Map.SectorDimension; } }

        #region Properties
        public Logic.Generators.SectorManager Manager { get { return Map.Context.Generator.SectorManager; } }
        public int[] SectorSeed { get; internal set; } 
        private List<MapEntity> entities;
        #endregion


        #region Readonly stuff - position related

        /// <summary> in world/sectorspace, the order of this sector along x and y axes</summary>
        public Coords Index { get; }
        
        /// <summary> world X and Y of our first tile </summary>        
        public Coords Offset { get; }

        public WorldMap Map { get; } 
        
        #endregion

        public Sector(WorldMap w, int x, int y) {
            Map = w;
            Index = new Coords(x, y);
            Offset = Index * TilesPerSector;
            entities = new List<MapEntity>();
        }

        public Tile this[int x, int y]
        {
            get {
                if (State != States.Simulated) return null;
                return Details.GetTile(x, y);
            }
            
        }

        public SectorSimulation Details { get; private set; }
        
        public SectorTypes Type { get; set; }

        #region State related
        public enum States {
            None,
            Unloaded,
            Simulated
        }

        private States state;
        public States State
        {
            get { return state; }
            set
            {
                if (state == value) return;
                var prevState = state;
                state = value;
                StateChanged?.Invoke(this, prevState, state);
                Map.SectorChangedState(this, prevState, state);
                
                if (state == States.Simulated) {
                    Details = new SectorSimulation(this);
                    Manager.SectorIsSimulated(this, Details);
                                        
                }
                if (prevState == States.Simulated) {
                    Manager.SectorNoLongerSimulated(this, Details);
                    Details = null;
                }
            }
        }
        
        public event Action<Sector, States, States> StateChanged;

        #endregion

        #region Entity containment
        /// <summary> Entity calls this </summary>        
        internal void LoseEntity(MapEntity e) {
            entities.Remove(e);
        }
        /// <summary> Entity calls this </summary>    
        internal void GainEntity(MapEntity e) {
            entities.Add(e);
        } 
        
        public IEnumerable<MapEntity> Entities { get { return entities; } }
        
        #endregion

    }
}
