﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ur.Grid;


namespace Skulls.Logic.Map {
    /// <summary> Sectors actively simulated in memory </summary>
    public class SectorSimulation {

        private Grid2D<Tile> tiles;

        Sector Sector { get;  }

        public SectorSimulation(Sector mySector) {
            Sector = mySector;
            ConstructTiles();
        }

        void ConstructTiles() {
            tiles = new Grid2D<Tile>(Sector.TilesPerSector, Sector.TilesPerSector, TileConstructor);
        }

        Tile TileConstructor(int x, int y) {
            var t = new Tile(Sector, x, y);
            return t;
        }

        internal Tile GetTile(int x, int y) {
            return tiles[x,y];
        }

        internal IEnumerable<Tile> GetAllTiles() { return tiles.GetAllTiles(); }
        
    }
}
