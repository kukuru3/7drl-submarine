﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skulls.Logic.Map {
    public enum SectorTypes {
        Undetermined,
        Deep,
        Medium,
        Shallow,
        Land,
    }
}
