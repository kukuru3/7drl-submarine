﻿using System;
using System.Collections.Generic;
using Ur.Grid;

namespace Skulls.Logic.Map {
    public class Tile {

        public Tile(Sector sector, int x, int y) {
            Sector = sector;
            SectorCoords = new Coords(x, y);           
        }

        /// <summary> Our coords relative to this sector </summary>
        public Coords SectorCoords  { get; }

        /// <summary> Reference to owning sector </summary>
        public Sector Sector        { get;  }

        /// <summary> Absolute world coordinates in tilespace </summary>
        public Coords WorldCoords   { get { return  Sector.Offset + SectorCoords;  } }

        //public int DrawSeed     { get; set; }
        //public int TerrainID    { get; set; }        
        //public float NoiseValue { get; set; }

        public DepthClass Depth { get; set; }
        public float      RawElevation;

        public float[] NoiseValues;
        public float   NoiseSum;

        
    }
}
