﻿using System;
using System.Collections.Generic;
using Ur.Grid;
using Ur;
using Alcove;
using Alcove.Light;
using System.Linq;

namespace Skulls.Logic.Map {
    /// <summary> The world is divided into SECTORS
    /// each sector is divided into tiles 
    /// At every single moment, up to 9 SECTORS are loaded and in memory </summary>    
    public class WorldMap {

        //public static int SectorsPerWorld { get { return Definitions.Instance.MapDefinition.SectorsPerWorld; } }

        public int SectorsWide      { get; private set; }
        public int SectorsTall      { get; private set; }
        public int SectorDimension  { get; private set; }        
        public long Identity        { get; set; } // unique world identity for use with serialization
        public bool Initialized     { get { return Identity > 0; } }

        public string Name          { get; set; }
        public int[] RNGSeed        { get; set; } // each world stores its master seed. It is used to derive individual Sector Seeds

        public Context Context { get; }

        public WorldMap(Context context) {
            Context = context;
        }
        
        private Grid2D<Sector> sectors;
        public Lightmap<float> Lightmap { get; private set; }
      
        /// <summary> constructs empty sector objects </summary>
        public void ConstructSectors(int w, int h, int dimension) {
            SectorsWide = w;
            SectorsTall = h;
            SectorDimension = dimension;
            sectors = new Grid2D<Sector>(SectorsWide, SectorsTall, SectorConstructor);            
        }
              
        Sector SectorConstructor(int x, int y) {
            var s = new Sector(this, x, y);
            return s;
        }

        /// <summary> Enumerates all sectors </summary>
        /// <returns></returns>
        internal IEnumerable<Sector> GetAllSectors() {
            return sectors.GetAllTiles();
        }

        internal Sector GetSectorByIndex(int x, int y) {
            return sectors?[x, y];
        }

        public Sector GetSectorAtTile(Coords worldCoords) {
            return GetSectorByIndex(
                worldCoords.X / SectorDimension,
                worldCoords.Y / SectorDimension
                );
        }
        
        /// <summary> Return tile at worldspace x, y </summary>        
        public Tile GetTileAtWorldspace(int x, int y) {
            var tps = SectorDimension;
            var sx = x / tps;
            var sy = y / tps;
            return sectors?[sx, sy]?[x - tps * sx, y - tps * sy ];
        }

        public void InitializeLightmap() {
            Lightmap = new Lightmap<float>(new Alcove.Light.Defaults.SimpleLightCalculator());
            Context.Generator.SectorManager.RegisterSystem(Lightmap);
        }

        internal void SectorChangedState(Sector sector, Sector.States prevState, Sector.States state) {
            
        }

        

        
    }
}
