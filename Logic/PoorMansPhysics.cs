﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ur.Grid;
using Ur.Geometry;
using Ur;
using static Skulls.Logic.DepthClass;

namespace Skulls.Logic {
    public class PoorMansPhysics {

        public class Collision {            
            public Coords[] Collided;
            public Vector2 CollisionPoint;            
        }

        static public Collision TryTraverseGrid(Vector2 A, Vector2 B, Predicate<Coords> collidesWith ) {

            var intersections = Calculate.IntersectLineWithUnitGrid(A, B);

            var cursor      = A;
            var cursorTile  = A.Truncate();

            Collision c = new Collision();

            // Cannot move at all since it is in an impassable location
            if (collidesWith(A.Truncate())) {                
                c.Collided = new[] { A.Truncate() };
                c.CollisionPoint = A;
            }

            foreach (var Z in intersections) { 

                var lc = new List<Coords>();
                var pt = GetPointType(Z);
            
                bool collided = false;
                var x = Z.x.Floor();
                var y = Z.y.Floor();

                if (pt == PointType.Corner) {
                    // get all the tiles surrounding the intersection
                    for (var yy = y - 1; yy <= y; yy++)
                    for (var xx = x - 1; xx <= x; xx++) {
                        var K = new Coords(xx, yy);
                        if (collidesWith(K)) {
                            lc.Add(K);
                            collided = true;
                        }
                    }
           
                } else if (pt == PointType.XAxis) {
                
                    for (var xx = x - 1; xx <= x; xx++ ) {
                        var K = new Coords(xx, y);
                        if (collidesWith(K)) {
                            lc.Add(K);
                            collided = true;
                        }
                    }
                } else if (pt == PointType.YAxis) {
                    for (var yy = y - 1; yy <= y; yy++) {
                        var K = new Coords(x, yy);
                        if (collidesWith(K)) {
                            lc.Add(K);
                            collided = true;
                        }
                    }
                }

                if (collided) {                
                    c.CollisionPoint = Z;
                    c.Collided = lc.ToArray();
                    return c;
                }
            }
            return null;

        }

        enum PointType {
            OutsideGrid,
            Corner,
            XAxis,
            YAxis,
        }

        static PointType GetPointType(Vector2 A) {
            var onX = A.x.Approximately(A.x.Floor());
            var onY = A.y.Approximately(A.y.Floor());
            if (onX && onY) return PointType.Corner;
            if (onX)        return PointType.XAxis;
            if (onY)        return PointType.YAxis;
            return PointType.OutsideGrid;
        }

        public PoorMansPhysics() {
            ConstructMovementMatrix();
        }

        private void ConstructMovementMatrix() {
            var md = (int)MaxDepth;
            MovementMatrix = new bool[md+1,md+1];            
        }

        void AllowMovement(DepthClass from, DepthClass to) {
            MovementMatrix[(int)from, (int)to] = true;
        }

        static public bool[,] MovementMatrix;



    }
}
