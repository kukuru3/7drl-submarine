﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;

namespace Skulls.Logic.Serialization {
    public static class FileOps {

        public const string GAME_ID = "Rogue Submarine";

        public static string GetWorldsFolder() {
            return Path.Combine(Ur.Filesystem.Folders.GetAppDataDirectory(), GAME_ID);
        }

        public static string GetSourceFolder() {
            return Ur.Filesystem.Folders.GetDirectory("");
        }

        public static bool CreateFolder(string folderName) {
            var path =  Path.Combine(GetWorldsFolder(), folderName );
            var di   =  Directory.CreateDirectory( path );
            return true;            
        }

        public static bool DestroyFolder(string folderName) {
            var path = Path.Combine(GetWorldsFolder(), folderName);
            var di = new DirectoryInfo(path);
            // potential security problem? 
            di.Delete(true);
            return true;
        }

        public static bool DestroyFile(string fileName) {
            var path = Path.Combine(GetWorldsFolder(), fileName);
            var fi = new FileInfo(path);
            fi.Delete();
            return true;
        }

        public static bool SaveDocumentToFile(XmlDocument document, string intermediateFolder, string filename ) {
            var path = Path.Combine(GetWorldsFolder(), intermediateFolder, filename);
            using (TextWriter tw = new StreamWriter(path)) {
                document.Save(tw);
                tw.Close();
            }
            return true;
            
        }

        /// <summary> Takes RELATIVE filename in the data folder </summary>
        public static XmlDocument LoadDocument(string filename) {
            var document = new XmlDocument();
            var path = Path.Combine(GetWorldsFolder(), filename);
            document.Load(path);
            return document;
        }
    }
}
