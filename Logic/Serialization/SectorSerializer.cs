﻿using System;
using System.Collections.Generic;
using System.Xml;
using Skulls.Logic.Map;

namespace Skulls.Logic.Serialization {
    /// <summary> Offloads a single sector into a single XML document </summary>
    public class SectorSerializer {

        XmlDocument doc;
        Sector sector;

        public XmlDocument OffloadSectorDetails(Sector s) {
           
            sector = s;

            doc = new XmlDocument();
            
            var decl = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            var root = doc.CreateElement("world");

            doc.AppendChild(root);
            doc.InsertBefore(decl, root);
            root = doc.DocumentElement;
            
            return doc;

        }


    }
}
