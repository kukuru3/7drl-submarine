﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Skulls.Logic;
using Skulls.Logic.Map;
using Skulls.Logic.Generators;

namespace Skulls.Logic.Serialization {
    public class WorldDeserializer {
        private XmlDocument document;
        private WorldMap       world;

        internal void FillWorldFromDocument(WorldMap world, XmlDocument doc) {
            document = doc;
            this.world = world;
            ConstructGeneralInfo();
            ConstructLayout();
        }

        private void ConstructGeneralInfo() {
            var root = document.DocumentElement;
            var seeds = new List<int>();
            var idString = root.SelectSingleNode("general/world_id").InnerText;
            foreach (XmlElement seedEl in root.SelectNodes("general/seed")) {
                var seedString = seedEl.InnerText;
                seeds.Add(int.Parse(seedString));
            }

            world.Identity = long.Parse(idString);
            world.RNGSeed  = seeds.ToArray();

        }

        void ConstructLayout() {

            var root = document.DocumentElement;
            var layoutEl = root.SelectSingleNode("sector_layout") as XmlElement;
            var ws = layoutEl.GetAttribute("sectors_wide") ?? "15";
            var hs = layoutEl.GetAttribute("sectors_tall") ?? "15";
            var ds = layoutEl.GetAttribute("sector_dim")  ?? "10";            
            world.ConstructSectors(
                int.Parse(ws), int.Parse(hs), int.Parse(ds)
            );
        }
    }
}
