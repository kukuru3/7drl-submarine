﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Skulls.Logic.Map;

namespace Skulls.Logic.Serialization {

    /// <summary> Serializes an entire world into a file (but not sector detail cache) </summary>
    public class WorldSerializer {
        
        XmlDocument document;
        Map.WorldMap worldMap;

        /// <summary> A world is serialized when it is created</summary>
        public XmlDocument SerializeToDoc(Map.WorldMap world) {
            Debug.Log("Serializing world...");

            var doc = document = new XmlDocument();
            this.worldMap = world;

            var decl = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            var root = doc.CreateElement("world");
            doc.AppendChild(root);
            doc.InsertBefore(decl, root);
            root = doc.DocumentElement;

            XmlElement generalElement = SerializeGeneralWorldData();
            root.AppendChild(generalElement);

            XmlElement sectorLayoutElement = SerializeSectorLayout();
            root.AppendChild(sectorLayoutElement);
            
            return doc;
            
        }
        
        XmlElement SerializeSectorLayout() {
            var slel = document.CreateElement("sector_layout");

                var attr_w = document.CreateAttribute("sectors_wide");
                attr_w.Value = worldMap.SectorsWide.ToString();
                slel.SetAttributeNode(attr_w);

                var attr_h = document.CreateAttribute("sectors_tall");
                attr_h.Value = worldMap.SectorsTall.ToString();
                slel.SetAttributeNode(attr_h); 

                var attr_dim = document.CreateAttribute("sector_dim");
                attr_dim.Value = worldMap.SectorDimension.ToString();
                slel.SetAttributeNode(attr_dim);

                //foreach (var sector in world.GetAllSectors()) {
                    //var seel = SerializeSingleSectorInLayout(sector);
                    //slel.AppendChild(seel);
                //}

            return slel;
        }

        XmlElement SerializeSingleSectorInLayout(Map.Sector sector) {

            Debug.Log("Serializing sector " + sector.Index.X + ":" + sector.Index.Y );

            var sel = document.CreateElement("sector");
            
            var attr_x = document.CreateAttribute("x");
            attr_x.Value = sector.Index.X.ToString();
            sel.SetAttributeNode(attr_x);

            var attr_y = document.CreateAttribute("y");
            attr_y.Value = sector.Index.Y.ToString();
            sel.SetAttributeNode(attr_y);
            
            return sel;
        }

        XmlElement SerializeGeneralWorldData() {

            Debug.Log("Serializing general world data");

            var generalElement = document.CreateElement("general");  
            
            var idel = document.CreateElement("world_id");
            idel.InnerText = worldMap.Identity.ToString();
            generalElement.AppendChild(idel);

            foreach (var item in worldMap.RNGSeed) {
                var seedEl = document.CreateElement("seed");
                seedEl.InnerText = item.ToString();
                generalElement.AppendChild(seedEl);
            }
          
            return generalElement;
        }

        
    }
}
