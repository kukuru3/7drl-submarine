﻿using System;
using System.Collections.Generic;

namespace Skulls.Logic {
    /// <summary> Terrain constants </summary>
    public class TerrainDef {

        public string   Name                { get; set; }
        public int[]   Chars                { get; set; }
        public int      ID                  { get; internal set; }
        public RLNET.RLColor  Color         { get; internal set; }
        public RLNET.RLColor  BackColor     { get; internal set; }
        public TerrainDef() { }

        
        
    }
}
