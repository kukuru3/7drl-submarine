﻿using System;
using System.Collections.Generic;

namespace Skulls.Logic {
    /// <summary> Also doubles as a scheduler </summary>
    public class WorldSim {

        Context Context { get; }
        public WorldSim(Context c) {
            Context = c;
        }

        /// <summary> Use this to implement blocking / non-blocking behaviour </summary>
        public bool SimulationActive { get; set;} =  true;
        float timeUntilNextTick   = 0f;        

        void AdvanceSimulatorClock(float time) {
            timeUntilNextTick -= time;
            if (timeUntilNextTick <= 0f) {
                timeUntilNextTick = Context.Definitions.SecondsBetweenSimTicks;
                WorldSimTick();                
            }
        }

        public event Action BeforeTick;
        public event Action AfterTick;
        
        
        public void WorldSimTick() {
            
            BeforeTick?.Invoke();
            foreach (var e in Context.Entities.Entities) { e.SimTick(); }
            Context.Entities.Cleanup();
            Context.Generator.SectorManager.UpdateReferenceFrame();
            AfterTick?.Invoke();        
        }
        
        
        /// <summary> Passage of time </summary>
        internal void Update(float time) {            
            if (SimulationActive) AdvanceSimulatorClock(time);               
        }

    }
}
