﻿using System;
using System.Collections.Generic;
using RLNET;

using Skulls.Rendering;

namespace Skulls.Pipeline {

    public enum HAlign {
        Left,
        Mid,
        Right
    }

    public class Button : PipelineElement {

        public string Text;
        public HAlign Alignment;
        public Action OnClick;
        public Action OnRClick;

        public bool    DrawBackground { get; set; }
        public RLColor BackColor { get; set; }

        public Button() {
            Flows = true;
        }

        internal override void Render() {
            if (DrawBackground) {
                foreach (var tile in Rect.Enumerate()) Renderer.Console.SetBackColor(tile.X, tile.Y, BackColor);                
            }
            var text = " " + Text + " ";
            float multiplier = 0.5f;
            if (Hilit) {
                text = (char)175 + Text + (char)174;
                multiplier = 1f;
            }

            Renderer.Console.Print(Rect.X0 , Rect.Y0, text, RLColor.White * multiplier );
        }

        public override void ProcessInput(InputSignals signal) {
            if (signal == InputSignals.Click) {
                OnClick?.Invoke();
            } else if (signal == InputSignals.AltClick) {
                OnRClick?.Invoke();
            }
        }

        bool Hilit { get { return Pipeline.ElementAtCoordinates(Pipeline.MousePosition) == this; } }
        
    }
}
