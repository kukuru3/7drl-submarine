﻿using System;

namespace Skulls.Pipeline.ControlPanel {

    public delegate void EffectorSignal(ControlPanelEffector sender, string signal_id, params object[] parameters);

    public class Cockpit : ControlPanelElement {
                
        internal void onSignalFromChild(ControlPanelEffector sender, string signalID, object[] parameters) {
            SignalGenerated?.Invoke(sender, signalID, parameters);
        }

        internal override void Render() {
            base.Render();
            DrawPack("cockpit");
        }

        public event EffectorSignal SignalGenerated;
    }
}