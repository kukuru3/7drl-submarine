﻿using System;
using System.Collections.Generic;
using RLNET;

namespace Skulls.Pipeline.ControlPanel {
    public enum Colors {
        CockpitBackground,
        GreyPanels,
        Paper,
        PaperLettering,
        BrassLetters,

        IronFinish,
        BikeLockVariation,
        BikeLockDark,

        LampUnlit,
        LampLitRed,
        LampLitYellow,
        LampLitGreen,
        LampBlueish,

        RecessedBrassLight,
        RecessedBrassShadow,

        ButtonSurface,

        EngravedLines,
        ViewportBackground,

        Black,
        White,
        Red,

        IronLit,
        DarkerCockpit,
        InteractibleHilite,
        IronDark,


        Max,
    }

    public static class Color {

        static Dictionary<Colors, RLColor> dict;

        static Color() {
            ConstructDictionary();
        }
        static void ConstructDictionary() {
            dict = new Dictionary<Colors, RLColor>();
            for (Colors t = 0; t < Colors.Max; t++) {
                dict.Add(t, FindColor(t));
            }
        }

        static private RLColor FindColor(Colors t) {
            return Config.Get<RLColor>(t.ToString());
        }
        static public RLColor Get(this Colors t) {
            return dict[t];
        }


    }
}
