﻿using System;
using System.Collections.Generic;


namespace Skulls.Pipeline.ControlPanel {
    public class ControlPanelEffector : ControlPanelElement{
        
        /// <summary> Identity is used to identify control panel elements by string, if necessary.
        /// Mostly, command relays use it to decypher events. Though there isn't a dictionary anywhere - they can share names. 
        /// example: speed_lever.lever_changed_position  </summary>
 
        public string SignalIdentity { get; internal set; }
        
        private Cockpit FindCockpitParent() {
            for (ControlPanelElement element = this; element != null; element = element.Parent as ControlPanelElement) {
                if (element is Cockpit) return element as Cockpit;
            }
            return null;
        }

        protected void GenerateRawSignal(string SignalType, params object[] parameters) {
            var cockpit = FindCockpitParent();
            var combinedSignalID = SignalIdentity + "." + SignalType;
            cockpit.onSignalFromChild(this, combinedSignalID, parameters);
        }
        
    }
}
