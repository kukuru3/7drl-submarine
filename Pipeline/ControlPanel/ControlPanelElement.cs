﻿using System;
using System.Collections.Generic;
using RLNET;
using Ur;
using Skulls.Rendering;
using Ur.Grid;

namespace Skulls.Pipeline.ControlPanel {

    public enum Embellishments {
        SurroundedByDarkPlate
    }

    public class ControlPanelElement : PipelineElement {

        
        protected void Embelish(Embellishments e) {
            switch(e) {
                case Embellishments.SurroundedByDarkPlate:
                    Utils.CreateBorderedRect(Colors.GreyPanels, Colors.CockpitBackground, Rect, false);
                    break;
            }
        }
            
        public bool BeingHeld { get; private set; }
        Coords previousDragPosition;    
        Coords MouseCoords { get {
            return new Coords(Console.Mouse.X, Console.Mouse.Y) ;
        } }

        #region Mouse processes
        protected virtual void MouseHoldStart(Coords atCoords) { }
        protected virtual void MouseDragged(Coords delta, Coords current) { }
        protected virtual void MouseReleased(Coords atCoords) { }
        protected virtual void MouseHeld(Coords atCoords) { }

        /// <summary>
        /// You do not need to call this. If you do not, your element will not be notified 
        /// of input mouse events. However, if you do call this, the events called will contain
        /// pixel coordinates, so be warned.
        /// Intended to replace ProcessMouse.
        /// </summary>
        protected void ProcessMouseRaw() {
            if (BeingHeld) {
                var delta = Pipeline.MouseDeltaRaw;
                if (delta.X != 0 || delta.Y != 0)   MouseDragged(delta, Pipeline.MousePosRaw);
                else                                MouseHeld   (Pipeline.MousePosRaw);
                if (!Console.Mouse.LeftPressed) {
                    MouseReleased(Pipeline.MousePosRaw);
                    Console.CursorVisible = true;
                    BeingHeld = false; CurrentlyHeld = null;                    
                }
            } else {
                if (CurrentlyHeld == null) {
                    if (!Pipeline.MouseIcky && Console.Mouse.LeftPressed) {
                        if (DragHotspot.Contains(MouseCoords)) {
                            MouseHoldStart(Pipeline.MousePosRaw);
                            Console.CursorVisible = false;
                            BeingHeld = true; CurrentlyHeld = this;
                        }
                    }
                }
            }
        }

        ///<summary>you do not need to call this. If you do not, your element will not be notified of mouse positions</summary>
        protected void ProcessMouse() {

            if (BeingHeld) {
                
                var delta = MouseCoords - previousDragPosition;
                if (delta.X != 0 || delta.Y != 0) MouseDragged(delta, MouseCoords);
                else MouseHeld(MouseCoords);
                previousDragPosition = MouseCoords;
                if (!Console.Mouse.LeftPressed) {
                    MouseReleased(MouseCoords);
                    Console.CursorVisible = true;
                    BeingHeld = false;
                    CurrentlyHeld = null;
                }
            } else {
                if (CurrentlyHeld == null) {
                    if (!Pipeline.MouseIcky && Console.Mouse.LeftPressed) {
                        if (DragHotspot.Contains(MouseCoords)) {
                            previousDragPosition = MouseCoords;
                            MouseHoldStart(MouseCoords);
                            Console.CursorVisible = false;
                            BeingHeld = true;
                            CurrentlyHeld = this;
                        }
                    }
                }
            }
        }

        /// <summary> DOES NOT CALL MouseReleased - the element can call it itself it it so desires</summary>
        protected void ElementForcesMouseRelease() {

            if (!BeingHeld) return;
            BeingHeld = false;
            CurrentlyHeld = null;
            Console.CursorVisible = true;
            Pipeline.MouseIcky = true;
        }

        static private ControlPanelElement CurrentlyHeld { get; set; }

        Rect DragHotspot
        {
            get
            {
                var l = LocalDragHotspot();
                return new Rect(
                    Rect.X0 + l.X0,
                    Rect.Y0 + l.Y0,
                    l.Width,
                    l.Height
                );
            }
        }

        protected virtual Rect LocalDragHotspot() {
            return new Rect(new Coords(0,0), Dimensions);
        } 
        #endregion

        #region Glyph printing

        /// <param name="x">LOCAL POSITION of glyph within this element</param>
        /// <param name="y">LOCAL POSITION of glyph within this element</param>
        public void PrintGlyph(int x, int y, int @char, Colors fore, Colors? back = null) {
            var sx = Rect.X0 + x; var sy = Rect.Y0 + y;
            if (back.HasValue) Console.SetBackColor(sx, sy, back.Value.Get());
            Console.SetChar(sx, sy, @char);
            var c = fore.Get();
            Console.SetColor(sx, sy, c);
        }
        public void PrintGlyph(int x, int y, Glyphs glyph, Colors fore, Colors? back = null) {
            //var sx = Rect.X0 + x; var sy = Rect.Y0 + y;
            PrintGlyph(x, y, (int)glyph, fore, back);
        }
        public void PrintText(int x, int y, string @string, Colors fore, Colors? back = null) {
            for (var i = 0; i < @string.Length; i++)
                PrintGlyph(x + i, y, (int)@string[i], fore, back);
        } 
        #endregion
        
        protected void DrawPack(string pack_id, int offsetX = 0, int offsetY = 0) {
            var pack = Data.RexPack.Get("cockpit");
            
            foreach (var item in pack.tiles.Iterate()) {
                Console.Set(offsetX + item.X, offsetY + item.Y, item.Value.Fore, item.Value.Back, item.Value.Char);
            }
        }
        

    }
}
