﻿using System;
using System.Collections.Generic;

namespace Skulls.Pipeline.ControlPanel {
    public abstract class ControlPanelIndicator : ControlPanelElement {

        public string IndicatorIdentity { get; set; }

        public abstract void SetIndicatorValueExternally(object value);


    }
}
