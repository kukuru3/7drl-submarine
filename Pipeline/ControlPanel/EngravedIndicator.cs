﻿using System;
using System.Collections.Generic;

namespace Skulls.Pipeline.ControlPanel {
    public class EngravedIndicator : ControlPanelElement {

        public bool Vertical { get; set; }
        
        public EngravedIndicator() {
            str = "";
        }

        string str;

        public void AppendChar(Glyphs glyph) {            
            AppendChar((char)glyph);
        }

        public void AppendChar(char @char) {
            str = str + @char;
        }

        internal override void Render() {
            base.Render();
            for (var i = 0; i < str.Length; i++) {
                PrintGlyph(Vertical ? 0 : i , Vertical ? i : 0, str[i], Colors.EngravedLines);
            }    
        }

    }
}
