﻿using System;
using System.Collections.Generic;
using Skulls.Rendering;
using Ur.Grid;
using System.Linq;
using Ur;

namespace Skulls.Pipeline.ControlPanel {
    public class Lever : ControlPanelEffector {

        int protrudeBefore;
        int protrudeAfter;

        /// <summary> coordinate of lever </summary>
        int currentPosition = 0;
        
        int holdStartStep = 0;
        int lastVisitedStep = 0;
        
        int actualValue = 0;        
        
        /// <summary> Values are dictionary keys, positions are dictionary values</summary>
        Dictionary<int, int> valuePositionDictionary;
        
        public int Dimension { get; }
        public bool Vertical { get; }        
        public bool AutoreleaseOnStepReached { get; set; } = false;

        public event Action<int, int> CanonicalPositionChanged;

        public int LeverValue {
            get { return actualValue; }
            private set {
                if (value == actualValue) return;
                var prev = actualValue;
                actualValue = value;
                CanonicalPositionChanged?.Invoke(prev, actualValue);
                GenerateRawSignal("Position", GetValueAtPosition(CurrentPosition));
            }
        }

        
        public int CurrentPosition {
            get { return currentPosition; }
            private set {
                value = value.Choke(0, Dimension -1);
                if (value == currentPosition) return;
                currentPosition = value;
            }
        }

        
        static public Lever CreateVertical(int height, int protrudeLeft = 0, int protrudeRight = 0) {
            var l = new Lever(height, true);
            l.protrudeBefore = protrudeLeft;
            l.protrudeAfter = protrudeRight;            
            return l;
        }
        static public Lever CreateHorizontal(int width) {
            var l = new Lever(width, false);
            return l;
            
        }
        
        private Lever(int dimension, bool vertical) {            
            Dimension = dimension;
            Vertical  = vertical;           
            valuePositionDictionary = new Dictionary<int, int>();
        }

        protected override Rect LocalDragHotspot() {
            if (Vertical)   return new Rect(- protrudeBefore, currentPosition, protrudeAfter + protrudeBefore + 1, 2);
            else    return new Rect(currentPosition, 0, 1, 1);
                        
        }

        internal override void Render() {

            base.Render();

            if (Vertical) RenderVertical();
            else          RenderHorizontal();
                        
            //foreach (var item in LocalDragHotspot().Enumerate())  PrintGlyph(item.X, item.Y, Glyphs.ArrowUp, Colors.Red);            
        }

        private void RenderHorizontal() {
            var x = 0; var y = 0;
            for (x= 0; x < Dimension; x++) {
                PrintGlyph(x, y, Glyphs.SubcellBottom, Colors.RecessedBrassShadow);
                PrintGlyph(x, y+1, Glyphs.SubcellTop, Colors.RecessedBrassLight);
            }
            x = currentPosition;
            PrintGlyph(x, y, Glyphs.TripleLine, Colors.Black, BeingHeld ? Colors.White : Colors.ButtonSurface);
            PrintGlyph(x, y+1, Glyphs.SubcellTop, Colors.Black);
        }

        void RenderVertical() {
            var x = 0; var y = 0;
            for (y = 0; y < Dimension; y++) {
                PrintGlyph(x, y, Glyphs.SubcellRight, Colors.RecessedBrassShadow, Colors.RecessedBrassLight);
            }
            y = currentPosition;
            for (x = 0 - protrudeBefore; x <= 0 + protrudeAfter; x++) {                 
                PrintGlyph(x, y, Glyphs.TripleLine, Colors.Black, BeingHeld ? Colors.White : Colors.ButtonSurface);
                var shadowGlyph = Glyphs.SubcellTop;
                if (x == 0 && y != Dimension-1) shadowGlyph = Glyphs.CheckerFull;
                PrintGlyph(x, y+1, shadowGlyph, Colors.RecessedBrassShadow );                
            }
        }


        // lever position can be dragged freely, when released it will automatically fall into the last step it touched
        public override void Update() {
            base.Update();
            ProcessMouse();            
        }



        protected override void MouseHoldStart(Coords atCoords) {            
            holdStartStep = CurrentPosition;
            lastVisitedStep = CurrentPosition;
        }

        protected override void MouseDragged(Coords delta, Coords current) {
            //Debug.Log("Mouse dragged at " + current.X + "," + current.Y + " [ " + delta.X + "," + delta.Y + "]");
            
            var prevPos = CurrentPosition;
            CurrentPosition += (Vertical ? delta.Y : delta.X);
            // find the nearest step to currentPosition 
            // that is situated between previous position, non-inclusive, and the new position, inclusive,
            var min = Numbers.Min(prevPos, CurrentPosition);
            var max = Numbers.Max(prevPos, CurrentPosition);

            var visitedSteps = valuePositionDictionary.Values.Where(s => s >= min && s <= max && s != prevPos).ToList();            
            if (visitedSteps.Count > 0) { 
                visitedSteps.Sort((a,b) => ((CurrentPosition - b).Abs() - (CurrentPosition - a).Abs()).Sign()  );
                if (AutoreleaseOnStepReached) visitedSteps = new List<int>(new[] { visitedSteps[0] });
                lastVisitedStep = visitedSteps.Last();
                foreach (var item in visitedSteps) { StepGrooveHit(item); }
            }
            
        }
        
        protected override void MouseReleased(Coords coords) { SnapLever(); }


        /// <summary> Triggered when ever a step groove is hit during lever drag</summary>        
        private void StepGrooveHit(int groovePosition) {            
            if(AutoreleaseOnStepReached) {
                SnapLever();
                ElementForcesMouseRelease();
            }
        }
        
        private void SnapLever() {
            var prevPosition = GetPositionOfValue(actualValue);

            if (prevPosition != lastVisitedStep) {
                //Debug.Log("Lever snaps to a new value " + lastVisitedStep);
            } else {
                //Debug.Log("Lever was not dragged far enough and snaps back to " + canonicalPosition);
            }
            CurrentPosition = lastVisitedStep;
            LeverValue  = GetValueAtPosition(CurrentPosition);            
        }

        public int GetPositionOfValue(int value) {
            return valuePositionDictionary[value];
        }

        int GetValueAtPosition(int pos) {
            return valuePositionDictionary.First(kvp => kvp.Value == pos).Key;
        }

        public void AddAllowedStep(int position, int value) {        
            if (valuePositionDictionary.ContainsKey(value)) throw new Exception("lever step with value " + value + " already exists" );
            if (valuePositionDictionary.ContainsValue(position)) throw new Exception("lever step at position " + position + " already exists " );
            valuePositionDictionary[value] = position;
            if (valuePositionDictionary.Count == 1) SetValueWithoutEvents(value);
        }

        /// <summary> Does not trigger events </summary> 
        internal void SetValueWithoutEvents(int v, bool animated = false) {
            if (animated) {
                throw new NotImplementedException();
            } else {
                actualValue = v;
                currentPosition = GetPositionOfValue(v);
            }
            
        }
        
    
    }
}
