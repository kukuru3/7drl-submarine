﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ur;
using Ur.Grid;
using RLNET;

namespace Skulls.Pipeline.ControlPanel {
        

    public class LinearGauge : ControlPanelIndicator {
        
        RLColor BackColor; RLColor LetterColor;
        int startThreshold;  int endThreshold;

        readonly int windowWidth;    readonly int windowHeight;

        private float displayedRibbonPosition;
        private float targetRibbonPos;
        private float ribbonVelocity;
        
        string[] contents;

        readonly bool     vertical;
        readonly bool     infinite;

        int colorFadeDistance = 999;
        float colorFadeMul    = 1f;

        private Dictionary<Coords, char> overlay = new Dictionary<Coords, char>();
        
        public LinearGauge(bool vertical, bool infinite, int length, int breadth) {
            if ((length % 2) * (length % 2) == 0) throw new Exception("EVEN NUMBER IN GAUGE? IMPOSSIBRU!");

            this.vertical = vertical;
            
            if (vertical) {
                windowWidth = breadth;
                windowHeight = length;
            } else {
                windowWidth = length;
                windowHeight = breadth;
            }
            
            contents = new string[breadth];
        }
        // special effects:
                    
        public void SetRibbonPosition(float newPos, bool immediate = true) {

            if (infinite)   newPos = newPos.Trunc();
            else            newPos = newPos.Choke(0f, 1f);

            targetRibbonPos = newPos;

            if (immediate) {
                displayedRibbonPosition = newPos;
                ribbonVelocity = 0f;
            }
        }
        
        /// <summary> Use this to add a persistent overlay character above ribbon contents 
        /// The coordinates are RELATIVE TO RIBBON CENTER</summary>
        public void AddOverlayChar(int dx, int dy, char character) {
            overlay.Add(new Coords(dx, dy), character);
        }
        
        public void SetColors(Colors letters = Colors.PaperLettering, Colors back = Colors.Paper ) {
            LetterColor = letters.Get();
            BackColor = back.Get();
        }

        /// <summary> Set ribbon string at row, starting from top</summary>
        public void SetRibbon(int row, string str) {
            contents[row] = str;
        }

        //public void SetRibbonChar(Rendering.Glyphs @char, int row, int position) {
            //contents[row][position] = (char)@char;
        //}

        public void SetColorFade(int startingDistanceFromCenter, float multiplier) {
            this.colorFadeDistance = startingDistanceFromCenter;
            this.colorFadeMul = multiplier;
        }

        public void SetThresholds(int startingOffset, int endingOffset) {
            startThreshold = startingOffset;
            endThreshold  = endingOffset;
        }
        
        public int RibbonwiseLength { get {
            return vertical ? windowHeight : windowWidth;
        } }

        public int RibbonwiseThickness { get {
                return vertical ? windowWidth: windowHeight;
         } }

        /// <summary>which character index coincides with the leftmost
        /// character of the window?</summary>        
        int GetRibbonCharOffset() {
            int charIndexAtMiddle = (startThreshold + (endThreshold - startThreshold) * displayedRibbonPosition).Round();
            return charIndexAtMiddle - RibbonwiseLength / 2;
        }

        float GetFadeMultiplierAt(int localX) {
            if (colorFadeMul.Approximately(1f)) return 1f;
            var delta = (RibbonwiseLength/ 2 - localX).Abs();
            delta = (delta - colorFadeDistance).Choke(0, windowWidth);
            if (delta == 0) return 1f;
            return (float)Math.Pow( colorFadeMul, delta);
        }

        public override void Update() {
            base.Update();
            Width = this.windowWidth;
            Height = this.windowHeight;
            DoNeedleDynamics();
        }

        private void DoNeedleDynamics() {
            
            displayedRibbonPosition += ribbonVelocity;

            // algo : project fastest possible stopping point. Does it fall before or after needle?
            
            var p = displayedRibbonPosition;
            var t = targetRibbonPos;

            if (infinite) {
                var dd = (p-t).Abs();
                if (dd > 0.5f) {
                    t = targetRibbonPos + (p-t).Sign(); // add or subtract one
                }
            }
            
            var d = t - p;            

            var v = ribbonVelocity;
            var a = 0.003f;
                        
            var z = v * v / a * v.Sign();
            var o =(p + z) - t ; // o is stopping point relative to target pos
            if (o < 0f) ribbonVelocity += a * 1.2f;
            if (o > 0f) ribbonVelocity -= a * 1.2f;

            ribbonVelocity = ribbonVelocity.Choke(-0.03f, 0.03f);
                        
        }

        internal override void Render() {
            base.Render();

            //Embelish(Embellishments.SurroundedByDarkPlate);

            var co = GetRibbonCharOffset();

            if (vertical) {
                for (var x = 0; x < windowWidth; x++) 
                for (var y = 0; y < windowHeight; y++) { 
                    char c = ' ';
                    var charAtRibbon = co+y;
                    if (charAtRibbon >= 0 && charAtRibbon < contents[x].Length) c= contents[x][charAtRibbon];
                    float mul = GetFadeMultiplierAt(y);
                    PrintChar(x, y, c, LetterColor * mul, BackColor * mul);
                }
            } else {
                for (var y = 0; y < windowHeight; y++)
                for (var x = 0; x < windowWidth; x++) {
                    char c = ' ';
                    var charAtRibbon = co+x;
                    if (charAtRibbon >= 0 && charAtRibbon < contents[y].Length) c= contents[y][charAtRibbon];
                    float mul = GetFadeMultiplierAt(x);
                    PrintChar(x, y, c, LetterColor * mul, BackColor * mul);
                }
            }
            foreach (var oc in overlay) {
                var x = oc.Key.X + windowWidth / 2;
                var y = oc.Key.Y + windowHeight / 2;
                PrintChar(x, y, oc.Value, RLColor.Black);
            }            
        }

        public override void SetIndicatorValueExternally(object value) {         
            var ts = value.GetType(); 
            float g = (float)value;
            SetRibbonPosition(g, false);
        }
    }
}
