﻿using System;
using System.Collections.Generic;
using Ur.Grid;
using Skulls.Rendering;
namespace Skulls.Pipeline.ControlPanel {
    public class Nameplate : ControlPanelElement {
        
        string Text;

        public Nameplate(string text) {            
            Text = text;
        }

        internal override void Render() {
            base.Render();
            // rect:
            Utils.CreateBorderedRect(
                Colors.GreyPanels, 
                Colors.GreyPanels, 
                new Rect(Rect.X0 - 1, Rect.Y0, Text.Length + 2, 1),
                false
            );
            // golden letters:

            PrintText(0, 0, Text, Colors.BrassLetters);
            
            // screws:
            PrintGlyph(-1, 0, Glyphs.SmallPoint, Colors.IronFinish );
            PrintGlyph(Text.Length, 0,Glyphs.SmallPoint, Colors.IronFinish );
        }


    }
}
