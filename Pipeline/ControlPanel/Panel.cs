﻿using System;
using System.Collections.Generic;

namespace Skulls.Pipeline.ControlPanel {
    public class Panel : ControlPanelElement {
        
        

        internal override void Render() {
            base.Render();

            Utils.FillRect(Colors.CockpitBackground, Rect);

            var bc = Colors.EngravedLines;

            var ul = Glyphs.Double_Corner_UpperLeft;
            var ll = Glyphs.Double_Corner_LowerLeft;
            var ur = Glyphs.Double_Corner_UpperRight;
            var lr = Glyphs.Double_Corner_LowerRight;

            PrintGlyph(1, 0, ul, bc);
            PrintGlyph(0, 1, ul, bc);
            PrintGlyph(1, 1, lr, bc);

            PrintGlyph(Width-2, 0, ur, bc);
            PrintGlyph(Width-1, 1, ur, bc);
            PrintGlyph(Width-2, 1, ll, bc);

            PrintGlyph(0, Height-2, ll, bc);
            PrintGlyph(1, Height-1, ll, bc);
            PrintGlyph(1, Height-2, ur, bc);

            PrintGlyph(Width-1, Height-2, lr, bc);
            PrintGlyph(Width-2, Height-1, lr, bc);
            PrintGlyph(Width-2, Height-2, ul, bc);

            for (var x = 2; x < Width-2; x++) {
                PrintGlyph(x, 0, Glyphs.Double_LineHorizontal, bc);
                PrintGlyph(x, Height-1, Glyphs.Double_LineHorizontal, bc);
            }
            for (var y = 2; y < Height-2; y++) {
                PrintGlyph(0, y,        Glyphs.Double_LineVertical, bc);
                PrintGlyph(Width-1,y,   Glyphs.Double_LineVertical, bc);
            }

            PrintGlyph(0,0, Glyphs.LargePoint, Colors.IronFinish);
            PrintGlyph(Width-1,0, Glyphs.LargePoint, Colors.IronFinish);
            PrintGlyph(0,Height-1, Glyphs.LargePoint, Colors.IronFinish);
            PrintGlyph(Width-1,Height-1, Glyphs.LargePoint, Colors.IronFinish);
            

        }

    }
}
