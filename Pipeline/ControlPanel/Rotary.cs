﻿using System;
using System.Collections.Generic;
using Ur.Grid;
using Ur;

namespace Skulls.Pipeline.ControlPanel {
    public class Rotary : ControlPanelEffector {
        
        /// <param name="width">in characters</param>
        /// <param name="offset">central character location</param>
        /// <param name="sensitivity">to mouse drags. Pixels needed to move by 1 char.</param>
        /// <param name="infinite"></param>
        public Rotary(int width, int offset, int sensitivity, bool infinite ) {
            Ribbon = " ";
            RotaryValue = 0;
            this.width  = width;
            this.offset = offset;
            this.infinite = infinite;
            this.pixelsPerMove = -sensitivity;
            this.Dimensions = new Coords(width, 2);
        }


        // logic variables : 
        private readonly int width;
        private readonly bool infinite; 
        private readonly int offset;
        private int value;


        // drag helper variables
        int pixelsPerMove = -3;        
        int totalDeltaX; 
        Coords whereStarted;


        public int RotaryValue {
            get { return value; }
            set {
                value = FixValue(value);
                if (this.value == value) return;
                var old = this.value;
                this.value = value;
                Changed?.Invoke(this, old, this.value);
            } }

        /// <summary> Fixes intended ribbon position to something legal for this rotary</summary>        
        private int FixValue(int value) {
            if (value < 0) {
                if (infinite) value += Ribbon.Length;
                else          value = 0;
            }
            if (value >= Ribbon.Length) {
                if (infinite) value -= Ribbon.Length;
                else value = Ribbon.Length - 1;
            }
            return value;
        }

        public string Ribbon { get; set; }

        bool hilit = false;

        public override void Update() {
            base.Update(); 
            ProcessWheel();    
            ProcessMouseRaw();
            ProcessTotalDelta();
        }

        private void ProcessWheel() {
            if (hilit) {
                var d = Pipeline.MouseWheelDelta;
                totalDeltaX += pixelsPerMove * d;
            }
        }


        #region Smooth drag control
        protected override void MouseHoldStart(Coords atCoords) {
            totalDeltaX = 0; whereStarted = atCoords;
        }

        protected override void MouseDragged(Coords delta, Coords current) {
            WrapMouse(current); totalDeltaX += delta.X;
        }

        protected override void MouseReleased(Coords atCoords) {
            Pipeline.ForceMousePosition(whereStarted);
        }


        void ProcessTotalDelta() {
            var num = totalDeltaX / pixelsPerMove;
            RotaryValue += num; totalDeltaX -= num * pixelsPerMove;
        }

        void WrapMouse(Coords mousePos) {
            var psx = Renderer.GlyphSize.X;
            var leftmost = psx * this.Rect.X0;
            var rightmost = (psx * this.Rect.X1) - 1;
            if (mousePos.X < leftmost) {
                Pipeline.ForceMousePosition(new Coords(rightmost, mousePos.Y));
            }
            if (mousePos.X > rightmost) {
                Pipeline.ForceMousePosition(new Coords(leftmost, mousePos.Y));
            }
            
        } 
        #endregion


        internal override void Render() {
            base.Render();
                    
            hilit = false;
            if (Rect.Contains(Pipeline.MousePosition) || BeingHeld)  hilit = true;

            // draw rotary:
            for (var i = 0; i < width; i++) {

                var cogColor = Colors.IronFinish;
                if (hilit) cogColor = Colors.InteractibleHilite;

                PrintGlyph(i, 0, Glyphs.Vertical4Stack, cogColor, Colors.IronDark);
                char c;
                if (infinite) { 
                    c = Ribbon[(i + RotaryValue + offset).Wrap(Ribbon.Length) ];
                } else {
                    var id = i + RotaryValue + offset;
                    if (id < 0 || id >= Ribbon.Length )  c = ' ';
                    else c = Ribbon[id];
                }
                PrintGlyph(i, 1, c, Colors.IronFinish, Colors.IronDark);
            }
        }

        public event Action<ControlPanelEffector, int, int> Changed;


        

    }
}
