﻿using System;
using System.Collections.Generic;
using Ur.Grid;

namespace Skulls.Pipeline.ControlPanel {
    public class Switch : ControlPanelEffector {

        public enum Styles {
            Default,
            GreyNullGreenPlus,            
        }


        public Switch(bool vertical, string id, Styles style = 0) {
            this.vertical = vertical;
            this.style = style;
            SignalIdentity = id;
            this.Dimensions = vertical ? new Coords(1,2) : new Coords(2,1);
        }

        readonly bool vertical;
        readonly Styles style;

        public bool State { get; private set; }

        public void SetState(bool newState, bool ignoreEvents = false) {
            if (this.State == newState) return;
            
            this.State = newState;
            if (!ignoreEvents) {
                GenerateRawSignal("Switch", State );
            }
        }
        
        void RenderKnob(int x, int y) {
            var backColor = State ? Colors.IronLit : Colors.IronFinish;
            if (MouseOver) backColor = Colors.InteractibleHilite;
            PrintGlyph(x,y, Glyphs.TripleLine, State ? Colors.White : Colors.Black, backColor);                        
        }

        void RenderNonKnob(int x, int y) {
            if (style == Styles.Default) { 
                PrintGlyph(x, y, vertical ? Glyphs.MidlineVerticalSubcell : Glyphs.MidlineHorizontalSubcell, Colors.EngravedLines, Colors.CockpitBackground);
            } else if (style == Styles.GreyNullGreenPlus) {
                PrintGlyph(x, y, 
                    State ? '+' : (char)Glyphs.NullAndVoid,
                    State ? Colors.LampLitGreen : Colors.IronFinish,
                    State ? Colors.IronFinish : Colors.GreyPanels);
            }
        }
        
        internal override void Render() {
            base.Render();
            if (MouseOver) {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Hand;
            }
            
            if (vertical) {
                RenderKnob(0, State?0:1);
                RenderNonKnob(0, State?1:0);
            } else { 
                RenderKnob(State?1:0, 0);
                RenderNonKnob(State? 0:1, 0);
            }
                        
        }

        bool MouseOver { get {
            return Rect.Contains(Pipeline.MousePosition);
        } }

        public override void ProcessInput(InputSignals signal) {
            base.ProcessInput(signal);
            if (signal == InputSignals.Click) {
                SetState(!State);
            } 
        }


    }
}
