﻿using System;
using System.Collections.Generic;
using Ur.Grid;
using Ur;
using RLNET;

using Skulls.Rendering;


namespace Skulls.Pipeline.ControlPanel {
    static public class Utils {
        
        static private RLNET.RLRootConsole console;
        static public RLNET.RLRootConsole Console { get {
            if (console == null) console = Config.Context.Renderer.Console;
            return console;
        }}


        static public void PrintGlyph(int x, int y, int @char, Colors fore, Colors? back = null) {
            Console.SetChar(x, y, @char);
            Console.SetColor(x,y, fore.Get());
            if (back.HasValue)
                console.SetBackColor(x, y, back.Value.Get());
        }
        static public void PrintGlyph(int x, int y, Glyphs glyph, Colors fore, Colors? back = null) {
            PrintGlyph(x, y, (int)glyph, fore, back);
        }
        static public void PrintText(int x, int y, string @string, Colors fore, Colors? back = null) {
            for (var i = 0; i < @string.Length; i++)
                PrintGlyph(x + i, y, (int)@string[i], fore, back);
        }

        static public void FillRect(Colors color, Rect rect) {
            foreach (var i in rect.Enumerate()) {
                Console.Print(i.X, i.Y, " ", Colors.Red.Get(), color.Get());
            }
        }

        /// <summary>Creates a rectangle with subcell borders</summary>
        /// <param name="padded">that one neat trick</param>
        static public void CreateBorderedRect(Colors borderColor, Colors rectColor, Rect rect, bool padded = false) {
            
            foreach (var i in rect.Enumerate()) {
                Console.Print(i.X, i.Y, " ",  Colors.PaperLettering.Get(), rectColor.Get());
            }

            for (var x = rect.X0; x <= rect.X1; x++) {
                var y = rect.Y0 - 1;
                if (padded) PrintGlyph(x, y, Glyphs.SubcellBottom, rectColor, borderColor);
                else        PrintGlyph(x, y, Glyphs.SubcellBottom, borderColor);
                y = rect.Y1 + 1;
                if (padded) PrintGlyph(x, y, Glyphs.SubcellTop, rectColor, borderColor);
                else        PrintGlyph(x, y, Glyphs.SubcellTop, borderColor);
            }

            var y0 = rect.Y0; var y1 = rect.Y1;
            if (padded) {  y0--; y1++; }
            for (var y = y0; y <= y1; y++) {
                var x = rect.X0 - 1;
                PrintGlyph(x, y, Glyphs.SubcellRight, borderColor);
                x = rect.X1 + 1;
                PrintGlyph(x, y, Glyphs.SubcellLeft, borderColor);
            }            
        }

    }
}
