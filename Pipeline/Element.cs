﻿using System;
using System.Collections.Generic;
using Ur.Grid;
using Ur;
using RLNET;

namespace Skulls.Pipeline {
    
    public enum InputSignals {
        None,
        MouseOver,
        Click,
        AltClick,
        Next,
        Previous
    }

    public abstract class PipelineElement {
                
        public object tagData;

        private PipelineElement parent;
        private List<PipelineElement> children;

        [System.ComponentModel.DefaultValue(true)]
        public bool Visible { get; set; }

        public bool Flows { get; set; }

        public IReadOnlyList<PipelineElement> Children { get { return children; } }

        /// <summary>For "flow" elements only - will automatically change height </summary>
        internal virtual void AdaptToWidth(int maxWidth, int startingY) {
            if (!Flows) return;
            Position = new Coords(Position.X, startingY);
            Dimensions = new Coords(maxWidth, Dimensions.Y);            
        }

        /// <summary> Set this in child class </summary>
        private Coords position;
        private Coords dimensions;       

        public PipelineElement() {
            children = new List<PipelineElement>();
            Visible  = true;
        }

        internal void Registered(RenderingPipeline pipeline) {
            Pipeline = pipeline;
        }
        
        public Coords Dimensions {
            get { 
                return dimensions;
            }
            set {
                dimensions = value;
            }
        }

        public Coords Position {
            get { 
                return position;
            }
            set {
                position = value;
            }
        }

        public int Width  { get { return dimensions.X; }   set { Dimensions = new Coords(value, Dimensions.Y); } }  
        public int Height { get { return dimensions.Y; }   set { Dimensions = new Coords(Dimensions.X, value); } }


        /// <summary> Final rect</summary>
        public Rect Rect { get {
            if (Parent == null) return new Rect(position, dimensions);
            var p = Parent.Rect;                
            return new Rect(
                p.X0 + position.X,
                p.Y0 + position.Y,
                dimensions.X,
                dimensions.Y
                );            
        } }
        
        public virtual void Update() { }
        
        #region Hierarchy management
        public PipelineElement Parent
        {
            get { return parent; }
            set
            {
                if (parent == value) return;
                SetParenthood(this, value);
            }
        }

        public RenderingPipeline  Pipeline { get; private set; }
        public Rendering.Renderer Renderer { get { return Pipeline.Context.Renderer; } }
        public RLRootConsole Console { get { return Pipeline.Context.Renderer.Console; } }

        void LoseChild(PipelineElement e) {
            children.Remove(e);
        }

        void GainedChild(PipelineElement e) {
            children.Add(e);
        }

         public bool IsParent(PipelineElement element) {
            for (PipelineElement p = Parent; p != null; p = p.Parent) if (p == element) return true;            
            return false;
        }
        
        static private void SetParenthood(PipelineElement e, PipelineElement newParent) {
            e.parent?.LoseChild(e);
            e.parent = newParent;
            e.parent?.GainedChild(e);
        }
        
        #endregion

        
        internal virtual void Render() { }
        
        public virtual void ProcessInput(InputSignals signal) {
            parent?.ProcessInput(signal);
        } 
        
        /// <summary> Prints a character at relative coordinates </summary>
        public void PrintChar(int relativeX, int relativeY, int character, RLColor? color = null, RLColor? backColor = null) {
            Renderer.Console.Set(Rect.X0 + relativeX, Rect.Y0 + relativeY, color, backColor, character  );
        }

        internal IEnumerable<PipelineElement> FlattenHierarchy() {
            var q = new Queue<PipelineElement>();
            q.Enqueue(this);
            while(q.Count > 0) {
                var el = q.Dequeue();
                foreach (var child in el.children) q.Enqueue(child);
                yield return el;
            }
        }

        internal void Die() {
            
        }

        public Coords LocalPosition(Coords source) {
            return source - Rect.BoundsLow;
        }
                
    }
}
