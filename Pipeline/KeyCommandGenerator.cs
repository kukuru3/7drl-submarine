﻿using System;
using System.Collections.Generic;

namespace Skulls.Pipeline {
    public class KeyCommandGenerator : PipelineElement{

        public event Action<string, object[]> SignalGenerated;

        void GenerateSignal(string signal_id, params object[] @params) {
            SignalGenerated?.Invoke(signal_id, @params);
        }

        public override void Update() {
            base.Update();            
            var kp = Pipeline.Context.Renderer.CurrentKeyPress;
            if (kp != null) {
                var boundCommand = Pipeline.Context.Definitions.GetKeybinding(kp.Key);
                if (boundCommand != null && boundCommand.Length > 0) {
                    GenerateSignal(boundCommand);
                }
            }
        }
    }
}
