﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skulls.Pipeline {
    public class Label : PipelineElement {
        public string Text { get; set;}
        

        public Label(string text = "") {
            Text = text;
            Flows = true;
        }

        internal override void Render() {
            base.Render();
            if (!Flows) { // render explicitly
                Pipeline.Context.Renderer.Console.Print(Rect.X0, Rect.Y0, Text, RLNET.RLColor.Black );
            }
        }
    }
}
