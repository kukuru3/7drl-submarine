﻿using System;
using System.Collections.Generic;
using RLNET;
using Skulls.Rendering;
using System.Linq;
using Ur.Grid;
using Ur.Geometry;

namespace Skulls.Pipeline {
    /// <summary> Contains all the pipeline elements and their items </summary>
    public class RenderingPipeline {

        public Context Context { get; }
        
        public  Root RootElement  { get; }        
        public Coords MousePosition { get; internal set; }
        private PipelineElement [,] coverage;

        private Renderer Renderer { get { return Context.Renderer; } }

        private OpenTK.GameWindow DirectWindowReference;

        public RenderingPipeline(Context context) {
            Context = context;            
            RootElement = new Root();
            RootElement.Position = new Coords();
            RootElement.Dimensions = new Coords(Renderer.Console.Width, Renderer.Console.Height);
            Register(RootElement);
            DirectWindowReference = Utilities.ExtractGameWindowReference(Renderer.Console);
            DirectWindowReference.MouseMove += ProcessRawMouseMovement; ;
            DirectWindowReference.MouseWheel += ProcessWheel;
        }

        private void ProcessWheel(object sender, OpenTK.Input.MouseWheelEventArgs e) {
            wheelPos = e.Value;
        }

        public Coords MousePosRaw   { get; private set; }
        public Coords MouseDeltaRaw { get; private set; }
        public int    MouseWheelDelta { get { return wheelPos - prevWheelPos; } }

        private int prevWheelPos;
        private int wheelPos;

        private void ProcessRawMouseMovement(object sender, OpenTK.Input.MouseMoveEventArgs e) {
            MousePosRaw = new Coords(e.Position.X, e.Position.Y);
            MouseDeltaRaw = new Coords(e.XDelta, e.YDelta);            
        }        
        /// <summary> In pixelspace  </summary>        
        internal void ForceMousePosition(Coords coords) {
            var offset = DirectWindowReference.PointToScreen(new System.Drawing.Point(coords.X, coords.Y));
            MouseDeltaRaw = new Coords(0,0);
            OpenTK.Input.Mouse.SetPosition(offset.X, offset.Y);
        }

        //public event EventHandler<OpenTK.Input.MouseMoveEventArgs> RawMouseMove;

        public void Register(PipelineElement el) {
            if (el.Parent == null && el != RootElement) el.Parent = RootElement;
            el.Registered(this);
        }
        
        void RebuildCoverage() {
            coverage = new PipelineElement[Renderer.Console.Width, Renderer.Console.Height];

            var Q = new Queue<PipelineElement>();
            Q.Enqueue(RootElement);
            while (Q.Count > 0) {                
                var el = Q.Dequeue();                
                if (el.Visible) {
                    if (el != RootElement) ApplyCoverage(el);            
                    foreach (var child in el.Children ) Q.Enqueue(child);                }
            }
    
        }

        private void ApplyCoverage(PipelineElement el) {            
            foreach (var tile in el.Rect.Enumerate()) {
                if (RootElement.Rect.Contains(tile)) {                   
                    coverage[tile.X, tile.Y] = el;
                }
            }
        }


        public PipelineElement ElementAtCoordinates(Coords c) {
            if (RootElement.Rect.Contains(c)) return coverage?[c.X, c.Y];
            return null;
        }

        public PipelineElement ElementAtCoordinates(int x, int y) {
            if (RootElement.Rect.Contains(x, y)) return coverage?[x,y];
            return null;
        }
        
        internal void Update() {
            
            RebuildCoverage();
            var Q = new Queue<PipelineElement>();
            Q.Enqueue(RootElement);
            while (Q.Count > 0) {
                var el = Q.Dequeue();
                el.Update();
                foreach (var child in el.Children ) Q.Enqueue(child);
            }
            if (MouseIcky && !Renderer.Console.Mouse.LeftPressed) MouseIcky = false;

            MouseDeltaRaw = new Coords();
            prevWheelPos = wheelPos;
        }

        internal void Render() {
            var Q = new Queue<PipelineElement>();
            Q.Enqueue(RootElement);
            while (Q.Count > 0) {                
                var el = Q.Dequeue();
                if (el.Visible) {
                    RenderElement(el);
                    foreach (var child in el.Children) Q.Enqueue(child);
                }
            }
        }
        
        private void RenderElement(PipelineElement el) {            
            el.Render();
        }

        internal void Destroy(PipelineElement e) {
            var children = e.FlattenHierarchy().ToList();
            children.Reverse();
            foreach (var child in children) {
                child.Parent = null;
                child.Die();                
            }            
            e.Die();
        }

        public bool MouseIcky { get; set; }
    }
}
