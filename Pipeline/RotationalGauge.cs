﻿using System;
using System.Collections.Generic;
using System.Text;
using RLNET;
using Ur;
using Ur.Grid;

namespace Skulls.Pipeline {
    public class RotationalGauge : PipelineElement{

        public RLColor BackColor;
        public RLColor LetteringColor;
        public RLColor IndicatorColor;

        public string[] RibbonStrings;

        /// <summary> How many fields of the ribbon are visible to the left and right </summary>
        public int     RibbonWindowHalfWidth { get; private set; }

        public int     Height { get; private set; }

        /// <summary> Total width of the ribbon </summary>
        public int     RibbonLength { get; private set; }
        public int     IndicatorPosition { get; internal set; }

        public bool     AcceptsInput { get; set; }

        public void SetDimensions(int ribbonLength, int windowHalfW, int height) {
            Height = height;
            RibbonLength = ribbonLength;
            RibbonStrings = new string[height];
            RibbonWindowHalfWidth = windowHalfW;
        }
        
        public void SetOffsetImmediate(int value) {
            RibbonOffset = value;
        }        
        

        public int RibbonOffset { get; private set; }
        
        bool holdInProgress;

        public override void ProcessInput(InputSignals signal) {
            base.ProcessInput(signal);
            
        }

        int prevDragX = 0;
        
        /// <param name="deltaX"></param>
        /// <param name="inProgress">false upon release</param>
        void Drag(int deltaX, bool inProgress) {
            RibbonOffset -= deltaX;
        }

        public override void Update()             {            
            base.Update();
            Dimensions = new Coords(RibbonWindowHalfWidth + 1, Height);
            //LocalRect = new Rect(LocalRect.X0, LocalRect.Y0, 2 * RibbonWindowHalfWidth + 1, Height);            

            if (!AcceptsInput) return;
            var mouse = Pipeline.Context.Renderer.Console.Mouse;
            if (holdInProgress) {                
                if (!mouse.LeftPressed) {
                    holdInProgress = false;
                    Drag(mouse.X - prevDragX, true);
                    prevDragX = mouse.X;
                    Pipeline.Context.Renderer.Console.CursorVisible = true;
                } else { // apply hold
                    Drag(mouse.X - prevDragX, false);
                    prevDragX = mouse.X;
                    //Debug.Log($"Click resumed at : {mouse.X},{mouse.Y}" );
                }
            } else {
                if (Rect.Contains(Pipeline.MousePosition)) { 
                    if (mouse.LeftPressed) { 
                        //Debug.Log($"CLICK START AT: {mouse.X},{mouse.Y}" );
                        prevDragX = Pipeline.MousePosition.X;
                        Pipeline.Context.Renderer.Console.CursorVisible = false;
                        holdInProgress = true;
                    }
                }
            
            }
        }

        string BuildString(int g) {
            var sb = new StringBuilder();
            
            var rpos = RibbonOffset;
            
            for (var cursor = -RibbonWindowHalfWidth; cursor <= RibbonWindowHalfWidth; cursor++ ) {
                var actualCharacterIndex = Numbers.Wrap(rpos + cursor, RibbonStrings[g].Length);
                sb.Append(RibbonStrings[g][actualCharacterIndex]);
            }
            
            return sb.ToString();
        }

        internal override void Render() {
            for (var g = 0; g < Height; g++) { 
                var str = BuildString(g);            
                for (var i = 0; i < str.Length; i++) {
                    var diff = Math.Abs((str.Length - (i * 2 + 1))) / 2;
                    diff -= RibbonWindowHalfWidth - 3;
                    float colorModifier = 1f;
                    var bcolor = BackColor;

                    if (diff == 1) colorModifier = 0.8f;
                    if (diff == 2) colorModifier = 0.65f;
                    if (diff == 3) colorModifier = 0.33f;
                    var @char = str[i];
                    var foreColor = LetteringColor * colorModifier;
                    
                    if (i == (IndicatorPosition - RibbonOffset + RibbonWindowHalfWidth).Wrap(RibbonLength) ) bcolor = IndicatorColor;

                    PrintChar(i, g, @char, foreColor, bcolor * colorModifier );
                }
            }
        }



    }
}
