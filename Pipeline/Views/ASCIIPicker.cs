﻿using System;
using System.Collections.Generic;
using Skulls.Rendering;
using RLNET;
using Ur.Grid;


namespace Skulls.Pipeline.Views {
    class ASCIIPicker : PipelineElement {

        public override void Update() {
           if (Renderer.CurrentKeyPress?.Key == RLKey.F10) {
                Visible = !Visible;
            }
        }

        Coords localMouseCoords;

        public override void ProcessInput(InputSignals signal) {
            
            if (signal == InputSignals.MouseOver) {
                localMouseCoords = LocalPosition(Pipeline.MousePosition);    
            } else if (signal == InputSignals.Click) {
                localMouseCoords = LocalPosition(Pipeline.MousePosition);
                var hilitAscii = (localMouseCoords.Y - 1) * 64 + (localMouseCoords.X - 1);
                PickedSymbol = hilitAscii;
            }
        }

        public int PickedSymbol { get; private set; }

        internal override void Render() {            
            Renderer.Console.SetBackColor(Rect.X0, Rect.Y0, Rect.Width, Rect.Height, RLColor.Gray * 0.5f );
            Renderer.Console.SetChar(Rect.X0, Rect.Y0, Rect.Width, Rect.Height, 32 );            
            
            for (var i = 0; i < 255; i++) {
                var y = i / 64; var x = i % 64;
                PrintChar(x + 1, y + 1, i, RLColor.White * 0.4f);
            }
            
            int hilitAscii = (localMouseCoords.Y - 1) * 64 + (localMouseCoords.X - 1);
            PrintChar(localMouseCoords.X, localMouseCoords.Y, hilitAscii, RLColor.White  );
                        
            Renderer.Console.SetColor(Rect.X0 + 2, Rect.Y0 + 6, RLColor.White );
            Renderer.Console.SetChar(Rect.X0 + 2, Rect.Y0 + 6, hilitAscii );
            Renderer.Console.Print(Rect.X0 + 5, Rect.Y0 + 6, $"({hilitAscii})", RLColor.Blend(RLColor.Red, RLColor.Gray, 0.2f) );
            Renderer.Console.SetChar(Rect.X1 - 3, Rect.Y0 + 6, PickedSymbol );
            Renderer.Console.SetColor(Rect.X1 - 3, Rect.Y0 + 6, RLColor.White);
            
        }
        
    }
}
