﻿using System;
using System.Collections.Generic;
using Ur;
using RLNET;

namespace Skulls.Pipeline.Views {
    public class Canvas : PipelineElement {

        internal ASCIIPicker MyPicker { get; set; }

        public int[,] canvas;
        public Canvas(int w, int h) {
            canvas = new int[w, h];
        }
        
        public override void ProcessInput(InputSignals signal) {
            var mousePos = LocalPosition(Pipeline.MousePosition);

            if (signal == InputSignals.Click) {
                if (new Ur.Grid.Rect(Position, Dimensions).Contains(mousePos)) {
                    canvas[mousePos.X, mousePos.Y] = MyPicker.PickedSymbol;
                }
            } else if (signal == InputSignals.AltClick) {
                canvas[mousePos.X, mousePos.Y] = ' ';
            }

        }

        internal override void Render() {
            foreach (var i in canvas.Iterate()) {
                this.PrintChar(i.X, i.Y, i.Value, RLColor.Gray);
            }
        }

    }
}
