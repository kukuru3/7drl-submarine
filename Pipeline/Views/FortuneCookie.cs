﻿using System;
using System.Collections.Generic;
using RLNET;

namespace Skulls.Pipeline.Views {
    public class FortuneCookie : PipelineElement {

        public FortuneCookie() {
            
        }

        double prevT = -100.0;

        public override void Update() {
            var t = Pipeline.Context.Game.WallClockTime;
            var delta = t - prevT;
            if (delta > 5.0) {
                CycleName();
                prevT = t;
            }
            
        }

        string Name = "";

        private void CycleName() {
            Name = Pipeline.Context.MnemhotepGenerator.Convert("$(germanic_names.person_name)");
        }

        internal override void Render() {
            //Pipeline.Context.Renderer.Console.SetBackColor(LocalRect.X0, LocalRect.Y0, LocalRect.Width, LocalRect.Height, RLColor.Gray * 0.4f);            
            Pipeline.Context.Renderer.Console.Print(Position.X, Position.Y, "Random name : " + Name, RLColor.Gray, RLColor.Gray * 0.4f );            
        }
    }
}
