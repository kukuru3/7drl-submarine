﻿using System;
using System.Collections.Generic;

namespace Skulls.Pipeline.Views {
    class GeneratorView : PipelineElement {

        public Logic.Generators.WorldGenerator Generator { get; }

        public GeneratorView(Logic.Generators.WorldGenerator generator) {
            Generator = generator;
        }
        internal override void Render() {
            
        }
    }
}
