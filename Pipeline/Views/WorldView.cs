﻿using System;
using System.Collections.Generic;
using Skulls.Logic.Map;
using Skulls.Logic.Entities;
using Skulls.Logic;
using Ur.Grid;
using Skulls.Rendering;
using RLNET;
using Alcove.Light;
using Ur;

namespace Skulls.Pipeline.Views {
    public class WorldView : PipelineElement {

        public WorldMap WorldMap { get { return Pipeline.Context.Map; } }
        public Lightmap<float> Lightmap { get { return Pipeline.Context.Map.Lightmap; } }

        public Camera MyCamera { get; internal set; }
        
        public override void Update() {
            if (!WorldMap.Initialized) return;            
        }

        internal override void Render() {
            if (!WorldMap.Initialized) return;
            if (MyCamera == null) return;            
            RenderTerrain();
            RenderEntities();
            
        }

        void RenderTerrain() { 
            
            var depth = MyCamera.Depth;
            var eye   = MyCamera.GetExtensor<Logic.Extensors.PlayerEyes>();
            bool periscopeMode = false;
            RenderRegime regime = new RenderRegime();

            if (MyCamera.Depth == Logic.DepthClass.Surface || ( (MyCamera.Depth == Logic.DepthClass.Shallow) && periscopeMode )) {
                regime.mode = Renderers.SurfaceRenderer;
                regime.ambientLight = 1f;
            } else {
                regime.mode = Renderers.WateryRenderer;
                if      (MyCamera.Depth == DepthClass.Shallow)  regime.ambientLight = 0.5f;
                else if (MyCamera.Depth == DepthClass.Deep)     regime.ambientLight = 0.08f;
                else                                            regime.ambientLight = 0f;
            }

            if (MyCamera.Depth > Logic.DepthClass.Shallow) {
                regime.mode = Renderers.WateryRenderer;
            }
            
            var viewportColor = new RLColor(0.03f, 0.025f, 0.05f);
            
            //for (var y = 0; y <= eye.Radius*2 + 1 ; y++)
            //for (var x = 0; x <= eye.Radius*2 + 1 ; x++) {
            //        var s =  new Coords(x+2, y+2);
            //        PrintChar(s.X, s.Y, ' ',  RLColor.Black, viewportColor);
            //}
            
            foreach (Coords t in eye.VisibleTiles) {
                var tile = WorldMap.GetTileAtWorldspace(t.X, t.Y);
                if (tile != null) {
                    var s = ProjectToLocal(t);
                    RenderTile(s.X, s.Y, tile, regime);
                }
            }
            
        }

        Coords ProjectToLocal(Coords world) {            
           var cameraStart = MyCamera.Location - new Coords(Dimensions.X/2, Dimensions.Y/2);
           return world - cameraStart;
            
        }

        void RenderEntities() {
            foreach (var s in Pipeline.Context.Generator.SectorManager.EnumerateSimulatedSectors()) {
                foreach (var e in s.Entities) {
                    var r = e.GetExtensor<Logic.Extensors.MapRenderInfo>();
                    if (r != null) {
                        var local = ProjectToLocal(e.Location);
                        r.Render(Rect.X0 + local.X, Rect.Y0 + local.Y);
                    }
                }
            }
        }

        RLColor Palette0 = RLColor.Blue * 0.2f;
        RLColor Palette1 = new RLColor(100, 220, 255) * 0.8f;

        RLColor Depths0 = RLColor.Blue * 0.2f;
        RLColor Depths1 = RLColor.Blue * 0.8f;

        private RLColor GetColorFromSurface(float rawValue, DepthClass depth) {
            if (depth < DepthClass.Surface) {
                if (rawValue > 0.8f) return RLColor.Green;
                return RLColor.Yellow;
                
            } else { // water
                float value = rawValue;
                if (depth == DepthClass.Shallow)    value += 0.05f;
                if (depth == DepthClass.MaxDepth)   value -= 0.05f;
                return RLColor.Blend(Palette1, Palette0, value);
            }
        }

        private RLColor GetColorFromDepth(float rawValue, DepthClass d) {
            if (d == DepthClass.Shallow)    rawValue += 0.15f;
            if (d == DepthClass.MaxDepth)   rawValue -= 0.15f;
            return RLColor.Blend(Depths1, Depths0, rawValue);
        }

        private void RenderTile(int sx, int sy, Tile tile, RenderRegime regime) {
            int glyph = 32;
            RLColor foreColor = RLColor.Black;
            RLColor backColor = RLColor.Red;

            var lightValue = regime.ambientLight + Lightmap.GetIntensityAt(tile.WorldCoords);
            lightValue = lightValue.Choke(0f, 1f);

            if (regime.mode == Renderers.SurfaceRenderer) {
                // no LOS blockers
                backColor = GetColorFromSurface(tile.RawElevation, tile.Depth);
            } else if (regime.mode == Renderers.WateryRenderer) {
                if (tile.Depth < MyCamera.Depth) backColor = RLColor.Black; 
                else backColor = GetColorFromDepth(tile.RawElevation, tile.Depth);                             
            }
            
            PrintChar(sx, sy, glyph, foreColor * lightValue, backColor * lightValue );
        }

        SeeEntityMode GetSeeMode(DepthClass observerDepth, DepthClass targetDepth) {
            if (observerDepth == targetDepth) return SeeEntityMode.SeeFull;
            if (observerDepth == DepthClass.Shallow && targetDepth == DepthClass.Surface) return SeeEntityMode.SeeWake;
            if (observerDepth >= DepthClass.Surface && targetDepth == DepthClass.Shallow) return SeeEntityMode.SeeShadow;
            return SeeEntityMode.DoNotSee;
        }

        enum SeeEntityMode {
            DoNotSee,
            SeeWake,
            SeeShadow,
            SeeFull
        }

        enum Renderers {
            None,
            SurfaceRenderer,
            WateryRenderer
        }

        class RenderRegime {
            public Renderers mode;
            public float     ambientLight = 0f;                        
        }

        public override void ProcessInput(InputSignals signal) {
            base.ProcessInput(signal);
            if (signal== InputSignals.AltClick || signal == InputSignals.Click) {

                var cameraStart = MyCamera.Location - new Coords(Dimensions.X/2, Dimensions.Y/2);

                var pos = LocalPosition(Pipeline.MousePosition);

                var worldPos = cameraStart + pos;

                WorldWasClicked?.Invoke(pos, worldPos, signal == InputSignals.AltClick);
                
           
            }
        }

        public delegate void WorldClick(Coords localCoords, Coords worldCoords, bool isRightClick);
        public event WorldClick WorldWasClicked;
    }
}
