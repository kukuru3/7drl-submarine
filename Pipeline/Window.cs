﻿using System;
using RLNET;

namespace Skulls.Pipeline {
    public class Window : PipelineElement {

        public RLColor? BackColor;

        internal override void Render() {
            
            if (BackColor.HasValue) {
                Renderer.Console.SetBackColor(Rect.X0, Rect.Y0, Rect.Width, Rect.Height, BackColor.Value);
            }
            Renderer.Console.SetChar(Rect.X0, Rect.Y0, Rect.Width, Rect.Height, 32);
            
        }

        public override void Update() {
            ReflowChildren();    
        }

        void ReflowChildren() {            
            var y = 0;
            foreach (var child in this.Children) {                
                child.AdaptToWidth(Width, y);                
                y += child.Dimensions.Y;
            }
        }

        public Label CreateLabel(string text) {
            var l = new Label();
            l.Height = 1;
            l.Width = this.Width;            
            l.Text = text;
            l.Parent = this;
            Pipeline.Register(l);
            return l;
        }

        public Button CreateButton(string text) {
            var b = new Button();
            b.Width = Width;
            b.Height = 1;
            b.Text = text;
            b.Parent = this;
            Pipeline.Register(b);
            return b;
        }


    }
}
