﻿using RLNET;

namespace Skulls {
    class Program {
        
        static void Main(string[] args) {
            
            Debug.Log("Welcome to [SUBMARINE_GAME]");
            Debug.Log("This is a debug window. It will not be present in the final version");
            var sg = new SkullsGame();
            sg.Run();
        }

        
        
    }
}
