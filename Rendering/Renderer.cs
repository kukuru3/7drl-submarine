﻿using System;
using System.Collections.Generic;
using RLNET;
using Skulls.Pipeline;
using Ur.Grid;


namespace Skulls.Rendering {
    public class Renderer {

        public Context Context { get; }
        
        /// <summary> Master generator for all rendering related purposes - NO LOGIC! </summary>
        public Ur.Random.Generator MasterRNG       { get; }        
        public RLRootConsole            Console     { get; }        

        RenderingPipeline Pipeline { get { return Context.Pipeline; } }

        public Renderer(Context context) {

            Context = context;            
            Console = Context.Game.RootConsole;
            MasterRNG = new Ur.Random.Generator();            
            prevMousePos = new Coords(-1, -1);
                        
        }

        /// <summary> The final size of a single character on screen, in pixels </summary>
        public Coords GlyphSize { get; private set;}

        internal void ProcessSettings(RLSettings settings) {
            GlyphSize = new Coords(
                (int)(settings.CharWidth * settings.Scale),
                (int)(settings.CharHeight * settings.Scale)
            );
        }

        Coords prevMousePos;

        public RLKeyPress CurrentKeyPress;

        public void Update(double time) {

            CurrentKeyPress = Console.Keyboard.GetKeyPress();
            
            if (CurrentKeyPress!= null) {
                switch (CurrentKeyPress.Key) {
                    case RLKey.Escape: Console.Close(); break;                    
                }
            }
            
            var mousePos = new Coords(Console.Mouse.X, Console.Mouse.Y);
                            
            if (mousePos != prevMousePos) {
                MouseMoved?.Invoke(prevMousePos, mousePos);                                          
            }
            
            Pipeline.MousePosition = mousePos;

            Pipeline.Update();

            Pipeline.ElementAtCoordinates(mousePos)?.ProcessInput(InputSignals.MouseOver);

            if (Console.Mouse.GetLeftClick()) 
                Pipeline.ElementAtCoordinates(mousePos)?.ProcessInput(InputSignals.Click);
            
            if (Console.Mouse.GetRightClick()) 
                Pipeline.ElementAtCoordinates(mousePos)?.ProcessInput(InputSignals.AltClick);
            
            Context.Simulator.Update((float)time);
            

            prevMousePos = mousePos;

        }
        
        public event Action<Coords, Coords> MouseMoved;
        
        public void Render(double time) {
            
            Console.Clear(
                
                32,                       
                Config.GetColor("CockpitBackground"),
                RLColor.White
                //Config.GetColor("DarkBrass"),
                //Config.GetColor("Brass")
            );

            Context.Pipeline.Render();
                        
            Console.Draw();
        }
        
    }
}
