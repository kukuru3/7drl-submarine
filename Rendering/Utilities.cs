﻿using System;
using System.Collections.Generic;
using Ur.Grid;
using System.Reflection;

namespace Skulls.Rendering {
    public static class Utilities {
        
        public static void PrintWrapped(Renderer renderer, int x, int y, string text, RLNET.RLColor borderBack, RLNET.RLColor borderFront, RLNET.RLColor contentBack, RLNET.RLColor contentLetters) {
            var c = renderer.Console;
            var width = text.Length;
            c.Print(x-1, y-1, "" + (char)201, borderFront, borderBack);
            for (var xx = 0; xx < width; xx++) c.Print(x+xx, y-1, "" + (char)205, borderFront, borderBack);
            c.Print(x+width, y-1, "" + (char)187, borderFront, borderBack);

            c.Print(x-1, y, ""+(char)186, borderFront, borderBack);
            c.Print(x,y, text, contentLetters, contentBack);
            c.Print(x+width, y, ""+(char)186, borderFront, borderBack);

            c.Print(x-1, y+1, "" + (char)200, borderFront, borderBack);
            for (var xx = 0; xx < width; xx++) c.Print(x+xx, y+1, "" + (char)205, borderFront, borderBack);
            c.Print(x+width, y+1, "" + (char)188, borderFront, borderBack);            
        }

        static private OpenTK.GameWindow extractedReference;

        static public OpenTK.GameWindow ExtractGameWindowReference(RLNET.RLConsole console) {
            if (extractedReference!=null) return extractedReference;
            var ti = console.GetType().GetTypeInfo();
            var fi = ti.GetField("window", BindingFlags.Instance |BindingFlags.NonPublic);
            var gw = fi.GetValue(console) as OpenTK.GameWindow;
            extractedReference = gw;
            return extractedReference;
        }

    }
}
