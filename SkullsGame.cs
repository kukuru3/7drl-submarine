﻿using System;
using RLNET;
using Skulls.Logic;
using Skulls.Rendering;
using Skulls.Logic.Generators;

namespace Skulls {
    public class SkullsGame {

        private const bool SHOW_GENERATOR_VIEW = false;

        public RLRootConsole        RootConsole { get; private set; }
        private Context             Context     { get;  set; }

        Data.DefinitionsLoader dloader = new Data.DefinitionsLoader();

        
        RLSettings GetSettings()
        {
            var settings = dloader.LoadSettings();            
            return settings;
        }

        void CreateContext() {

            var settings = GetSettings();

            RootConsole = new RLRootConsole(settings);
            Context = new Context(this);            
            Context.Renderer.ProcessSettings(settings);
            
            if (SHOW_GENERATOR_VIEW) {
                Context.GUI.CreateWorldGeneratorView();
            }
            
            RootConsole.Update += UpdateRootConsole;
            RootConsole.Render += RenderRootConsole;
            RootConsole.OnLoad += Loaded;            
        }

     
        private void Loaded(object sender, EventArgs e) {
            
            var defs = dloader.LoadDefinitions();

            Context.Definitions = defs; 

            dloader.LoadBlueprints();
            dloader.LoadConfig();
            dloader.LoadKeyBindings();
            dloader.LoadRexPacks();

            var m = new Data.MnemhotepLoader();
            m.DoLoad(Mnemhotep.Mnemhotep.Instance);
            
            if (!SHOW_GENERATOR_VIEW) {
                var c = new WorldCreator();
                var gendefs = Context.Definitions.GeneratorDefinition;
                foreach (var w in c.EnumerateExistingWorlds())  c.DestroyWorldFiles(w);                
                var world = c.CreateNewWorld(Context, 
                    gendefs.WorldSectorsWide, 
                    gendefs.WorldSectorsTall, 
                    gendefs.TilesPerSector
                );
                Context.AssignWorld(world);                
                Context.Generator.WorldStarter.GenerateAll();
            }
            
        }

        private void RenderRootConsole(object sender, UpdateEventArgs e) {
            Context.Renderer.Render(e.Time);
        }

        private void UpdateRootConsole(object sender, UpdateEventArgs e) {
            WallClockTime += e.Time;
            Context.Renderer.Update(e.Time);
        }
        
        public void Run() {
            CreateContext();
            RootConsole.Run();
        }

        public double WallClockTime { get; private set; }

    }
}
